package br.com.kayros.modelo.controleacesso;

import br.com.kayros.arquitetura.anotation.EntityProperties;
import br.com.kayros.arquitetura.modelo.impl.EntidadeAuditavelImpl;
import br.com.kayros.arquitetura.util.Constantes;
import br.com.kayros.enums.controleacesso.PerfilUsuarioStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(schema = Constantes.SCHEMA_BANCO, name = "tb_perfil_usuario")
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfilUsuario extends EntidadeAuditavelImpl {

    @Column
    private String descricao;

    @Enumerated(EnumType.STRING)
    @Column(name = "perfil_usuario_status")
    private PerfilUsuarioStatus perfilUsuarioStatus = PerfilUsuarioStatus.ATIVO;

    @EntityProperties(targetEntity = PerfilUsuarioModulo.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "perfilUsuario", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<PerfilUsuarioModulo> modulos;

    @JsonIgnore
    public boolean isPerfilAtivo() {
        return PerfilUsuarioStatus.ATIVO == perfilUsuarioStatus;
    }
}



