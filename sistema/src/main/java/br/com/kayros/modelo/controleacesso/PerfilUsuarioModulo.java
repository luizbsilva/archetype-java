package br.com.kayros.modelo.controleacesso;

import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.modelo.impl.EntidadeImpl;
import br.com.kayros.arquitetura.util.Constantes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(schema = Constantes.SCHEMA_BANCO, name = "tb_perfil_usuario_modulo")
@EqualsAndHashCode
public class PerfilUsuarioModulo extends EntidadeImpl {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_perfil_usuario", nullable = false)
    private PerfilUsuario perfilUsuario;

    @Enumerated(EnumType.STRING)
    private Modulo modulo;

    @ElementCollection(targetClass = Funcionalidade.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "tb_funcionalidade_modulo", joinColumns = @JoinColumn(name = "perfil_usuario_modulo_id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    @Enumerated(EnumType.STRING)
    Set<Funcionalidade> funcionalidades;


}
