package br.com.kayros.modelo.controleacesso;

import br.com.kayros.arquitetura.modelo.impl.EntidadeImpl;
import br.com.kayros.arquitetura.util.Constantes;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(schema = Constantes.SCHEMA_BANCO, name = "tb_acesso")
@EqualsAndHashCode
public class Acesso extends EntidadeImpl {

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "data_hora")
    private Date dataHora;

    @CreationTimestamp
    @Column(name = "horario_acesso")
    private LocalTime horarioAcesso;


    @Column(name = "ip_acesso")
    private String ip;

    @ManyToOne
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;

}



