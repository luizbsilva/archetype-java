package br.com.kayros.modelo.controleacesso;

import br.com.kayros.arquitetura.anotation.EntityProperties;
import br.com.kayros.arquitetura.deserializer.StringRawDeserializer;
import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.enums.Status;
import br.com.kayros.arquitetura.modelo.impl.EntidadeAuditavelImpl;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.arquitetura.util.Constantes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.impl.Log4JLogger;

import javax.persistence.*;
import java.io.IOException;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(schema = Constantes.SCHEMA_BANCO, name = "tb_usuario")
@EqualsAndHashCode
public class Usuario extends EntidadeAuditavelImpl implements AuthIdentifier {

    @Column(name = "admin")
    private Boolean admin = false;

    @Column(name = "primeiro_acesso")
    private Boolean primeiroAcesso = false;

    @Column(name = "nome")
    private String nome;

    @Column(name = "login")
    private String login;

    @Column(name = "senha")
    private String senha;

    @Column(name = "email")
    private String email;

    @Column(name = "telefone")
    private String telefone;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "setor")
    private String setor;

    @Column(name = "data_nascimento")
    private Date dataNascimento;


    @Enumerated(EnumType.STRING)
    @Column(name = "usuario_status")
    private Status usuarioStatus = Status.ATIVO;

    @EntityProperties(value = {
            "id",
            "criadoPor",
            "criadoEm",
            "atualizadoEm",
            "atualizadoPor",
            "descricao",
            "perfilUsuarioStatus",
            "status"
    })
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_perfil_usuario")
    private PerfilUsuario perfilUsuario;

    @JsonIgnore
    @OneToMany(mappedBy = "usuario", cascade = {CascadeType.REMOVE})
    private Set<Acesso> acessos;

    @Transient
    private String token;

    @Transient
    private String senhaAntesDeEncriptar;

    @Transient
    private Boolean exibirTelaAjuda;

    @JsonDeserialize(using = StringRawDeserializer.class)
    @Transient
    private Map<Modulo, Set<Funcionalidade>> funcionalidadesPorModulo = new EnumMap<>(Modulo.class);

    @Override
    public Long getIdentifier() {
        return getId();
    }

    @Override
    public String getIdentifierName() {
        return this.getNome();
    }

    @JsonIgnore
    public boolean isUsuarioAtivo() {
        return Status.ATIVO == usuarioStatus;
    }

    @Override
    public boolean isAtivo() {
        return Status.ATIVO == this.getStatus();
    }

    @Override
    public boolean isNew() {
        return this.getId() == null;
    }

    public void setFuncionalidadesPorModulo(String funcionalidadesPorModulo) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            this.funcionalidadesPorModulo = mapper.readValue(funcionalidadesPorModulo, new TypeReference<Map<Modulo, Set<Funcionalidade>>>() {
            });
        } catch (IOException ignored) {
            new Log4JLogger().error(ignored.getMessage());
        }

    }

}
