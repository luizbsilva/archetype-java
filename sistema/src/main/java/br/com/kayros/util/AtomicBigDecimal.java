package br.com.kayros.util;

import java.math.BigDecimal;

public class AtomicBigDecimal {

    private BigDecimal bigDecimal;

    public AtomicBigDecimal() {
        bigDecimal = BigDecimal.ZERO;
    }

    public AtomicBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }

    public BigDecimal get() {
        return bigDecimal;
    }

    public void add(BigDecimal bigDecimal) {
        this.bigDecimal = this.bigDecimal.add(bigDecimal);
    }

    public void subtract(BigDecimal bigDecimal) {
        this.bigDecimal = this.bigDecimal.subtract(bigDecimal);
    }
}
