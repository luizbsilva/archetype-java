package br.com.kayros.enums.controleacesso;

public enum PerfilUsuarioStatus {
    INATIVO,
    ATIVO;

    public PerfilUsuarioStatus ifAtivo(Runnable runnable) {
        return check(ATIVO, runnable);
    }

    public PerfilUsuarioStatus ifInativo(Runnable runnable) {
        return check(INATIVO, runnable);
    }

    private PerfilUsuarioStatus check(PerfilUsuarioStatus e, Runnable runnable) {
        if (this.equals(e)) {
            runnable.run();
        }
        return this;
    }
}
