package br.com.kayros.dto.controleacesso;

import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import br.com.kayros.arquitetura.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UsuarioDTO {

    @ProjectionProperty("admin")
    private Boolean admin;

    @ProjectionProperty("login")
    private String login;

    @ProjectionProperty("nome")
    private String nome;

    @ProjectionProperty("id")
    private Long id;

    @ProjectionProperty("cpf")
    private String cpf;

    private String email;

    @ProjectionProperty("criadoEm")
    private Date criadoEm;

    @ProjectionProperty("usuarioStatus")
    private Status usuarioStatus;

    @ProjectionProperty("setor")
    private String setor;
}
