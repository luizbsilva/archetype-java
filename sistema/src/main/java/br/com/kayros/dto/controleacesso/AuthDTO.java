package br.com.kayros.dto.controleacesso;

import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import br.com.kayros.arquitetura.deserializer.StringRawDeserializer;
import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Transient;
import java.io.IOException;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Slf4j
public class AuthDTO implements AuthIdentifier {

    @ProjectionProperty("id")
    private Long id;

    @ProjectionProperty("criadoEm")
    private Date criadoEm;

    @ProjectionProperty("admin")
    private Boolean admin;

    @ProjectionProperty("login")
    private String login;

    @ProjectionProperty("nome")
    private String nome;

    @ProjectionProperty("token")
    private String token;

    @ProjectionProperty("primeiroAcesso")
    private Boolean primeiroAcesso;

    @JsonDeserialize(using = StringRawDeserializer.class)
    @Transient
    private Map<Modulo, Set<Funcionalidade>> funcionalidadesPorModulo = new EnumMap<>(Modulo.class);

    public void setFuncionalidadesPorModulo(String funcionalidadesPorModulo) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            this.funcionalidadesPorModulo = mapper.readValue(funcionalidadesPorModulo,
                    new TypeReference<Map<Modulo, Set<Funcionalidade>>>() {
                    });
        } catch (IOException ignored) {
            log.error(ignored.getMessage());
        }

    }

    @Override
    public Long getIdentifier() {
        return getId();
    }

    @Override
    public String getIdentifierName() {
        return this.getNome();
    }

    public AuthDTO(Long id, Date criadoEm, Boolean admin, String login, String nome, Boolean primeiroAcesso) {
        super();
        this.id = id;
        this.criadoEm = criadoEm;
        this.admin = admin;
        this.login = login;
        this.nome = nome;
        this.primeiroAcesso = primeiroAcesso;

    }

}
