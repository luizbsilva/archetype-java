package br.com.kayros.dto.controleacesso;

import br.com.kayros.arquitetura.anotation.Join;
import br.com.kayros.arquitetura.anotation.ProjectionConfigurationDTO;
import br.com.kayros.arquitetura.anotation.ProjectionProperties;
import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import br.com.kayros.arquitetura.dto.impl.EntidadeDTO;
import br.com.kayros.modelo.controleacesso.Usuario;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@ProjectionConfigurationDTO(
        joins = {
                @Join(associacao = "usuario", apelido = "usuario")
        }
)
public class AcessoDTO extends EntidadeDTO {

    @ProjectionProperty("dataHora")
    private Date dataHora;

    @ProjectionProperty("id")
    private String ip;

    @ProjectionProperties(values = {"id", "nome"})
    private Usuario usuario;

}
