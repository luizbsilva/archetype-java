package br.com.kayros.dto.controleacesso;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioNovaSenhaDTO {
    private Long id;

    private String senhaAtual;

    private String novaSenha;
}
