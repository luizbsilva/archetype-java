package br.com.kayros.dto.controleacesso;

import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import br.com.kayros.arquitetura.dto.impl.EntidadeAuditavelDTO;
import br.com.kayros.arquitetura.enums.Status;
import br.com.kayros.enums.controleacesso.PerfilUsuarioStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class PerfilUsuarioDTO extends EntidadeAuditavelDTO {


    @ProjectionProperty("status")
    private Status status;

    @ProjectionProperty("descricao")
    private String descricao;

    @Enumerated(EnumType.STRING)
    @ProjectionProperty("perfilUsuarioStatus")
    private PerfilUsuarioStatus perfilUsuarioStatus;
}
