package br.com.kayros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EntityScan(basePackages = Aplicacao.PACOTE_MODELO)
@EnableJpaRepositories(Aplicacao.PACOTE_DAO)
@EnableScheduling
public class Aplicacao {

    public static final String PACOTE_MODELO = "br.com.kayros.modelo";
    public static final String PACOTE_DAO = "br.com.kayros.dao";

    public static void main(String[] args){
        SpringApplication.run(Aplicacao.class, args);
    }

}
