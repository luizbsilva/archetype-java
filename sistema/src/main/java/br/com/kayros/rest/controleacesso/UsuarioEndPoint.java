package br.com.kayros.rest.controleacesso;

import br.com.kayros.arquitetura.rest.CrudEndPoint;
import br.com.kayros.arquitetura.service.Service;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.dto.controleacesso.UsuarioDTO;
import br.com.kayros.modelo.controleacesso.Usuario;
import br.com.kayros.service.controleacesso.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

@Component
@Path("/controle-acesso/usuario")
public class UsuarioEndPoint extends CrudEndPoint<Usuario, Long> {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    protected Service<Usuario> getServico() {
        return usuarioService;
    }

    @POST
    @Path("/ativar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ativar(Usuario usuario) {
        return onExecuteService(usuario, () -> usuarioService.ativar(usuario));
    }

    @POST
    @Path("/inativar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inativar(Usuario usuario) {
        return onExecuteService(usuario, () -> usuarioService.inativar(usuario));
    }

    @POST
    @Path("/confirmar-dados-autenticacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmarDadosAutenticacao(Map<String, String> parametros) {
        String login = parametros.get("login");
        String senha = parametros.get("senha");

        return onExecuteService(new Usuario(), new OnExecuteServiceCallBack() {
            @Override
            public void executar() throws RegraNegocioException {
                usuarioService.confirmarDadosAutenticacao(login, senha);
            }

            @Override
            public String getMensagem() {
                return null;
            }
        });
    }

    @PermitAll
    @POST
    @Path("/recuperar-senha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response recuperarSenha(UsuarioDTO dto) {
        usuarioService.recuperarSenha(dto);
        return Response.ok().build();
    }

    @PermitAll
    @GET
    @Path("/obter-por-login/{login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterPorLogin(@PathParam("login") String login) {
        return Response.ok(usuarioService.getByLogin(login)).build();
    }

    @PermitAll
    @GET
    @Path("/obter-por-login-ultimo-acesso/{login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterPorLoginUltimoAcesso(@PathParam("login") String login) {
        return Response.ok(usuarioService.buscarUltimoAcesso(login)).build();
    }

    @PermitAll
    @POST
    @Path("/salvar-nova-senha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarNovaSenha(Usuario usuario) {
        return onExecuteService(usuario, () -> usuarioService.salvarNovaSenha(usuario));
    }
}
