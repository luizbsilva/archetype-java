package br.com.kayros.rest.security;

import br.com.kayros.arquitetura.rest.auth.AuthFilter;
import br.com.kayros.modelo.controleacesso.Usuario;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.ext.Provider;
import java.util.Set;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthFilterImpl extends AuthFilter<Usuario> {

    @Override
    protected boolean isUserAllowed(Usuario usuario, Set<String> rolesSet) {
        return true;
    }

}
