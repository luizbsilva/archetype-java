package br.com.kayros.rest.util;

import br.com.kayros.arquitetura.rest.BaseEndPoint;
import br.com.kayros.arquitetura.util.MapBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/util")
@Component
public class UtilEndPoint extends BaseEndPoint {

    @Value("${kayros.versao}")
    private String versao;

    @GET
    @Path("/ip")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIp() {
        return Response.ok(MapBuilder.create("ip", getRequest().getRemoteAddr()).build()).build();
    }

    @PermitAll
    @GET
    @Path("/sistema/versao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response versao() {
        return Response.ok(MapBuilder.create("versao", versao).build()).build();
    }

}
