package br.com.kayros.rest.controleacesso;

import br.com.kayros.arquitetura.rest.CrudEndPoint;
import br.com.kayros.arquitetura.service.Service;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import br.com.kayros.service.controleacesso.PerfilUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/controle-acesso/perfil-usuario")
public class PerfilUsuarioEndPoint extends CrudEndPoint<PerfilUsuario, Long> {

    @Autowired
    private PerfilUsuarioService perfilUsuarioService;

    @Override
    protected Service<PerfilUsuario> getServico() {
        return perfilUsuarioService;
    }

    @POST
    @Path("/ativar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response ativar(PerfilUsuario perfilUsuario) {
        return onExecuteService(perfilUsuario, () -> perfilUsuarioService.ativar(perfilUsuario));
    }

    @POST
    @Path("/inativar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inativar(PerfilUsuario perfilUsuario) {
        return onExecuteService(perfilUsuario, () -> perfilUsuarioService.inativar(perfilUsuario));
    }
}
