package br.com.kayros.rest.security;

import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.rest.auth.AuthEndPoint;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.dto.controleacesso.AuthDTO;
import br.com.kayros.modelo.controleacesso.Usuario;
import br.com.kayros.service.controleacesso.AcessoService;
import br.com.kayros.service.controleacesso.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import java.nio.file.AccessDeniedException;
import java.util.Objects;

@Component
@Path("auth")
public class AuthEndPointImpl extends AuthEndPoint {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private AcessoService acessoService;

    @Override
    protected AuthIdentifier processarLogin(String username, String password) throws AccessDeniedException {

        Usuario usuario = usuarioService.processarLogin(username, password);

        if (Objects.isNull(usuario)) {
            throw new AccessDeniedException(MensagemI18N.getKey("usuario.login.falha.regra1"));
        }

        this.executeAfterLogin(usuario);

        return createResponseAuth(usuario);
    }

    private AuthIdentifier createResponseAuth(Usuario usuario) {

        AuthDTO authDTO = new AuthDTO(usuario.getId(), usuario.getCriadoEm(), usuario.getAdmin(), usuario.getLogin(), usuario.getNome(), usuario.getPrimeiroAcesso());
        if (usuario.getPerfilUsuario() != null) {
            usuario.getPerfilUsuario().getModulos().forEach(perfilModulo -> {
                Modulo modulo = perfilModulo.getModulo();
                if (!perfilModulo.getFuncionalidades().isEmpty()) {
                    authDTO.getFuncionalidadesPorModulo().put(modulo, perfilModulo.getFuncionalidades());
                }

            });
        }

        return authDTO;

    }


    private void executeAfterLogin(Usuario usuario) {
        acessoService.registrarAcesso(usuario, request.getRemoteAddr());
        usuario.setExibirTelaAjuda(acessoService.countPorUsuario(usuario.getId()) <= 1);
    }

}
