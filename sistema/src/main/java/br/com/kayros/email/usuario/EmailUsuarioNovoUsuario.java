package br.com.kayros.email.usuario;

import br.com.kayros.arquitetura.email.Email;
import br.com.kayros.arquitetura.email.EmailBodyBuilder;
import br.com.kayros.modelo.controleacesso.Usuario;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class EmailUsuarioNovoUsuario implements Email {

    @Override
    public String getAssunto() {
        return "Primeiro acesso ao Sistema - Orientações";
    }

    @Override
    public String getId() {
        return "e6efc9a4-452f-4dd3-896b-5d9e9044bcd9";
    }

    @NonNull
    private Usuario usuario;

    @Override
    public String getTextoEmail() {
        return EmailBodyBuilder.build()
                .addText("Olá!")
                .addLineBreak(2)
                .addText("Por ser o primeiro acesso ao sistema, seguem usuário e senha para login.")
                .addLineBreak(2)
                .addTextStrong("Usuário: ").addText("${login}")
                .addLineBreak()
                .addTextStrong("Senha: ").addText("${senha}")
                .addLineBreak(2)
                .addText("Após esse login, você será direcionado para uma tela de Alteração de senha.")
                .addLineBreak(2)
                .addText("Crie uma nova senha e clique em Salvar.")
                .addLineBreak(2)
                .addTextStrong("Em caso de dúvidas entre em contato com o departamento de TI")
                .addLineBreak(2)
                .addTextStrong("Atenção: Este é um e-mail automático. Favor não responder.")
                .addLineBreak(2)
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        List<String> emails = new ArrayList<>();

        emails.add(usuario.getEmail());

        return emails;
    }

    @Override
    public Map<String, String> getParametros() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put("${login}", usuario.getLogin());
        parametros.put("${senha}", usuario.getSenhaAntesDeEncriptar());

        return parametros;
    }
}
