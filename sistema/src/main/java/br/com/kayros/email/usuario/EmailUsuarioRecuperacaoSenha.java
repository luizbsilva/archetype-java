package br.com.kayros.email.usuario;

import br.com.kayros.arquitetura.email.Email;
import br.com.kayros.arquitetura.email.EmailBodyBuilder;
import br.com.kayros.modelo.controleacesso.Usuario;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public class EmailUsuarioRecuperacaoSenha implements Email {

    @NonNull
    private Usuario usuario;

    @Override
    public String getId() {
        return "30674804-27a6-46e7-9d4f-118a2958517b";
    }

    @Override
    public String getAssunto() {
        return "Solicitação de alteração de senha do Sistema.";
    }

    @Override
    public String getTextoEmail() {
        return EmailBodyBuilder.build()
                .addText("Olá!")
                .addLineBreak(2)
                .addText("Sua senha foi alterada para:")
                .addTextStrong(usuario.getSenhaAntesDeEncriptar())
                .addLineBreak(2)
                .addText("Para acessar o sistema ").addLinkSistema("clique aqui.")
                .addLineBreak(2)
                .addText("Após o login utilizando essa senha, você será direcionado para uma tela de alteração de senha.")
                .addLineBreak()
                .addText("Crie uma nova senha e clique em salvar.")
                .addLineBreak(2)
                .addTextStrong("Em caso de dúvidas entre em contato com o departamento de TI.")
                .addLineBreak(2)
                .addTextStrong("Atenção: Este é um e-mail automático. Favor não responder.")
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        return Collections.singletonList(usuario.getEmail());
    }
}
