package br.com.kayros.dao.controleacesso;

import br.com.kayros.arquitetura.persistencia.impl.DAOImpl;
import br.com.kayros.dto.controleacesso.AcessoDTO;
import br.com.kayros.modelo.controleacesso.Acesso;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Component
public class AcessoDAO extends DAOImpl<Acesso> {

    @Override
    protected Class<AcessoDTO> getDTOClass() {
        return AcessoDTO.class;
    }

    public Long countPorUsuario(Long usuarioId) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Acesso> root = criteriaQuery.from(Acesso.class);

        criteriaQuery.where(criteriaBuilder.equal(root.join("usuario").get("id"), usuarioId));

        criteriaQuery.select(criteriaBuilder.countDistinct(root.get("id")));

        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    public Date ultimoAcessoUsuario(Long usuarioId) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Date> criteria = criteriaBuilder.createQuery(Date.class);
        Root<Acesso> from = criteria.from(Acesso.class);

        criteria.select(from.get("dataHora"));
        criteria.where(this.montarWhereParaUltimoAcessoUsuario(from, usuarioId));
        criteria.orderBy(criteriaBuilder.desc(from.get("horarioAcesso")));

        final TypedQuery<Date> query = this.getEntityManager().createQuery(criteria);
        query.setMaxResults(1);

        return query.getSingleResult();
    }

    private Predicate[] montarWhereParaUltimoAcessoUsuario(Root<Acesso> from, Long usuarioId) {
        final CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
        final List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(from.join("usuario").get("id"), usuarioId));
        predicates.add(cb.lessThan(from.get("horarioAcesso"), LocalTime.of(LocalTime.now().getHour(), LocalTime.now().getMinute())));

        return predicates.toArray(new Predicate[predicates.size()]);
    }
}
