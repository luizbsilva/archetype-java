package br.com.kayros.dao.controleacesso;

import br.com.kayros.arquitetura.persistencia.impl.DAOImpl;
import br.com.kayros.dto.controleacesso.PerfilUsuarioDTO;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


@Repository
@Component
public class PerfilUsuarioDAO extends DAOImpl<PerfilUsuario> {


    @Override
    protected Class<PerfilUsuarioDTO> getDTOClass() {
        return PerfilUsuarioDTO.class;
    }

    @Override
    protected void beforeSalvarAlterar(PerfilUsuario perfilUsuario) {

        perfilUsuario.getModulos().forEach(modulo -> modulo.setPerfilUsuario(perfilUsuario));
    }

    @Override
    public PerfilUsuario get(Long id) {

        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<PerfilUsuario> criteriaQuery = criteriaBuilder.createQuery(PerfilUsuario.class);
        Root<PerfilUsuario> root = criteriaQuery.from(PerfilUsuario.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("id"), id));
        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        TypedQuery<PerfilUsuario> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return getSingleResult(typedQuery);

    }

}
