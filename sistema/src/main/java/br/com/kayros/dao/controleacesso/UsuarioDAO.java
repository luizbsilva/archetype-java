package br.com.kayros.dao.controleacesso;

import br.com.kayros.arquitetura.dto.PaginacaoResultado;
import br.com.kayros.arquitetura.enums.Status;
import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import br.com.kayros.arquitetura.persistencia.impl.DAOImpl;
import br.com.kayros.arquitetura.util.AliasToBeanNestedResultTransformer;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.arquitetura.util.Util;
import br.com.kayros.dto.controleacesso.UsuarioDTO;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import br.com.kayros.modelo.controleacesso.Usuario;
import org.hibernate.Hibernate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
@Component
public class UsuarioDAO extends DAOImpl<Usuario> {

    @Override
    protected Class<UsuarioDTO> getDTOClass() {
        return UsuarioDTO.class;
    }

    @Override
    protected void beforeSalvarAlterar(Usuario usuario) {
        ajustarUsuario(usuario);
    }

    public Usuario consultarPeloCodigoSei(Integer codigoUsuarioSei) {
        return getWithProjections((Specification<Usuario>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(createJoin(root, "usuarioSEI").get("codigo"), codigoUsuarioSei));
    }

    @Override
    public void excluir(Long id) throws RegraNegocioException {
        beforeExcluir(id);
        super.excluir(id);
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void beforeExcluir(Long id) throws RegraNegocioException {
        if ((boolean) getValorAtributo("admin", id)) {
            RegraNegocioException.build(MensagemI18N.getKey("usuario.regraNegocio.naoPodeExcluirAdmin")).lancar();
        }
    }

    private void ajustarUsuario(Usuario usuario) {
        if (usuario.isNew()) {
            String senha = Util.gerarSenha(8);
            usuario.setSenhaAntesDeEncriptar(senha);
            usuario.setSenha(Util.encriptSHA256(senha));
        }
    }

    public Usuario processarLogin(String username) {
        return consultarLogin(username, null);
    }

    public Usuario processarLogin(String username, String password) {
        return consultarLogin(username, password);
    }

    public Usuario consultarLogin(String login, String senha) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Usuario> criteriaQuery = criteriaBuilder.createQuery(Usuario.class);
        Root<Usuario> root = criteriaQuery.from(Usuario.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("login"), login));
        predicates.add(criteriaBuilder.equal(root.get("usuarioStatus"), Status.ATIVO));
        predicates.add(criteriaBuilder.equal(root.get("status"), Status.ATIVO));

        if (!StringUtils.isEmpty(senha)) {
            String passwordEncripted = Util.encriptSHA256(senha);
            predicates.add(criteriaBuilder.equal(root.get("senha"), passwordEncripted));
        }

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        TypedQuery<Usuario> typedQuery = getEntityManager().createQuery(criteriaQuery);

        Usuario usuario = getSingleResult(typedQuery);
        if (usuario != null) {
            Hibernate.initialize(usuario.getPerfilUsuario());
        }
        return usuario;
    }

    @Override
    public PaginacaoResultado<UsuarioDTO> dtoListar(Paginacao paginacao) {
        PaginacaoResultado<UsuarioDTO> paginacaoResultado;

        long contar = dtoContar(paginacao);

        if (contar > 0) {
            Class<UsuarioDTO> dtoClass = getDTOClass();

            CriteriaBuilder criteriaBuilder = criteriaBuilder();
            CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            criteriaQuery.distinct(true);

            Root root = criteriaQuery.from(Usuario.class);

            createJoinsFromAnottation(root, dtoClass);
            addSelecionsByDTO(criteriaBuilder, criteriaQuery, root, dtoClass);
            addWhereInCriteriaQuery(paginacao, criteriaBuilder, criteriaQuery, root);
            addOrdenacaoPorPaginacao(paginacao, criteriaBuilder, criteriaQuery, root);

            TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(criteriaQuery);
            addPaginacaoInTypeQuery(paginacao, typedQuery);

            List<UsuarioDTO> dtos = typedQuery.getResultList()
                    .stream()
                    .map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, dtoClass))
                    .collect(Collectors.toList());

            paginacaoResultado = new PaginacaoResultado<>(dtos, contar);

        } else {
            paginacaoResultado = new PaginacaoResultado<>(Collections.emptyList(), 0);

        }

        return paginacaoResultado;
    }

    public long dtoContar(Paginacao paginacao) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root root = criteriaQuery.from(Usuario.class);

        createJoinsFromAnottation(root, getDTOClass());
        criteriaQuery.select(criteriaBuilder.countDistinct(root.get("id")));
        addWhereInCriteriaQuery(paginacao, criteriaBuilder, criteriaQuery, root);

        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    public void confirmarDadosAutenticacao(String login, String senha) throws RegraNegocioException {
        Usuario loginView = consultarLogin(login, senha);

        if (Objects.isNull(loginView)) {
            throw new RegraNegocioException(MensagemI18N.getKey("usuario.regraNegocio.dadosAutenticacaoNaoConferem"));
        }
    }

    public void salvarNovaSenha(Usuario usuario) {
        usuario.setSenha(Util.encriptSHA256(usuario.getSenha()));
        usuario.setPrimeiroAcesso(false);
        alterarAtributos(usuario, "senha", "primeiroAcesso");
    }

    public Boolean buscarUsuarioPorPerfil(PerfilUsuario perfilUsuario) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Usuario> criteria = criteriaBuilder.createQuery(Usuario.class);
        Root<Usuario> root = criteria.from(Usuario.class);
        criteria.select(root);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("perfilUsuario"), perfilUsuario));

        criteria.where(predicates.toArray(new Predicate[0]));

        final TypedQuery<Usuario> query = this.getEntityManager().createQuery(criteria);

        return query.getResultList().isEmpty();
    }
}
