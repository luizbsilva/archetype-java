package br.com.kayros.service.controleacesso.menuacao;


import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifierContext;
import br.com.kayros.arquitetura.service.impl.MenuAcaoServiceImpl;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PerfilUsuarioMenuAcaoService extends MenuAcaoServiceImpl<PerfilUsuario> {

    @Override
    public Modulo getModulo() {
        return Modulo.PERFIL_USUARIO;
    }

    protected void adicionarAcoesPaginaConsultar(PerfilUsuario perfilUsuario, List<MenuAcao> acoes) {
        acoes.add(getAcaoVoltar());

        if (perfilUsuario.isPerfilAtivo()) {
            acoes.add(getAcaoEditar());
        }

        perfilUsuario.getPerfilUsuarioStatus()
                .ifAtivo(() -> acoes.add(getAcaoInativar()))
                .ifInativo(() -> acoes.add(getAcaoAtivar()));

        acoes.add(getAcaoExcluir());
    }

    protected void adicionarAcoesPaginaCadastro(PerfilUsuario perfilUsuario, List<MenuAcao> acoes) {
        acoes.add(getAcaoCancelar());

        if (perfilUsuario.isNew() || perfilUsuario.isPerfilAtivo()) {
            acoes.add(getAcaoSalvar());
        }
    }


    public List<MenuAcao> getAcoesTabelaTelaListagem(PerfilUsuario perfilUsuario) {
        List<MenuAcao> acoes = new ArrayList<>();
        AuthIdentifier usuarioLogado = AuthIdentifierContext.getCurrentAuthIdentifier();

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.ATIVAR_INATIVAR)) {
            if (perfilUsuario.isPerfilAtivo()) {
                acoes.add(getAcaoInativarTabelaListagem());
            } else {
                acoes.add(getAcaoAtivarTabelaListagem());
            }
        }

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.EXCLUIR)) {
            acoes.add(super.getAcaoExcluirTabelaListagem());
        }

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.CONSULTAR) || contemPermissao(Funcionalidade.CADASTRAR)) {
            acoes.add(super.getAcaoConsultarTabelaListagem());
        }

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.EDITAR)) {
            acoes.add(super.getAcaoEditarTabelaListagem());
        }

        return acoes;
    }
}
