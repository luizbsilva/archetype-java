package br.com.kayros.service.comum;

import br.com.kayros.arquitetura.email.EmailSenderConfiguration;
import br.com.kayros.arquitetura.email.EmailSenderService;
import br.com.kayros.arquitetura.enums.ConfiguracaoEmailProtocolo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl extends EmailSenderService {

    @Autowired
    private Environment springEnv;

    @Override
    public EmailSenderConfiguration getConfiguracao() {

        return new EmailSenderConfiguration() {
            @Override
            public String getHost() {
                return springEnv.getProperty("email.host");
            }

            @Override
            public String getPorta() {
                return springEnv.getProperty("email.porta");
            }

            @Override
            public String getRemetente() {
                return springEnv.getProperty("email.remetente");
            }

            @Override
            public String getUsuario() {
                return springEnv.getProperty("email.usuario");
            }

            @Override
            public String getSenha() {
                return springEnv.getProperty("email.senha");
            }

            @Override
            public ConfiguracaoEmailProtocolo getProtocolo() {
                return ConfiguracaoEmailProtocolo.SSL;
            }

            @Override
            public String getUrlSistema() {
                return springEnv.getProperty("kayros.url");
            }
        };
    }
}
