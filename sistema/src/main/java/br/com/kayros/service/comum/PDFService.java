package br.com.kayros.service.comum;

import br.com.kayros.arquitetura.util.Util;
import com.openhtmltopdf.pdfboxout.PdfBoxRenderer;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.OutputStream;

@Slf4j
@Service
public class PDFService {

    public static final String E_COMERCIAL = "E_COMERCIAL";

    public void gerarPdfDeHtml(String html, OutputStream output) {
        try {
            html = StringEscapeUtils.unescapeHtml4(html);
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withHtmlContent(getHtmlConteudoPadronizado(html), "");
            builder.toStream(output);

            PdfBoxRenderer pdfBoxRenderer = builder.buildPdfRenderer();
            pdfBoxRenderer.createPDFWithoutClosing();

            PDDocument document = pdfBoxRenderer.getPdfDocument();


            document.save(output);
            document.close();
        } catch (Exception ignored) {
            log.error(ignored.getMessage(), ignored);
        }
    }


    private String getHtmlConteudoPadronizado(String html) {
        StringBuilder htmlConteudo = new StringBuilder();
        htmlConteudo.append("<html>");
        htmlConteudo.append("<head>");
        htmlConteudo.append("<style>");
        htmlConteudo.append("@page {");
        htmlConteudo.append("margin: 2cm 1cm");
        htmlConteudo.append("}");
        htmlConteudo.append("body {");
        htmlConteudo.append("font-family: sans-serif;");
        htmlConteudo.append("font-size: 11px;");
        htmlConteudo.append("}");

        htmlConteudo.append(".ql-font-serif{");
        htmlConteudo.append("font-family: serif;");
        htmlConteudo.append("}");
        htmlConteudo.append(".ql-font-monospace{");
        htmlConteudo.append("font-family: monospace;");
        htmlConteudo.append("}");
        htmlConteudo.append(".ql-font-times{");
        htmlConteudo.append("font-family: \"Times New Roman\";");
        htmlConteudo.append("}");

        htmlConteudo.append(".ql-align-center{");
        htmlConteudo.append("text-align: center;");
        htmlConteudo.append("}");
        htmlConteudo.append(".ql-align-right{");
        htmlConteudo.append("text-align: right;");
        htmlConteudo.append("}");
        htmlConteudo.append(".ql-align-justify{");
        htmlConteudo.append("text-align: justify;");
        htmlConteudo.append("}");

        htmlConteudo.append("li{");
        htmlConteudo.append("padding-left: 1.5em;");
        htmlConteudo.append("}");
        htmlConteudo.append("li.ql-indent-1{");
        htmlConteudo.append("padding-left: 4.5em;");
        htmlConteudo.append("}");
        htmlConteudo.append("li.ql-indent-2{");
        htmlConteudo.append("padding-left: 7.5em;");
        htmlConteudo.append("}");

        htmlConteudo.append("span.espaco{");
        htmlConteudo.append("margin-left: 5px;");
        htmlConteudo.append("}");

        htmlConteudo.append("div.quebra-pagina{");
        htmlConteudo.append("page-break-after: always;");
        htmlConteudo.append("width: 100%;");
        htmlConteudo.append("color: #FFF;");
        htmlConteudo.append("}");

        htmlConteudo.append(".table-descontos{");
        htmlConteudo.append("width: 100%;");
        htmlConteudo.append("border: 1px solid;");
        htmlConteudo.append("border-collapse: collapse;");
        htmlConteudo.append("}");
        htmlConteudo.append(".table-descontos th{");
        htmlConteudo.append("text-align: center;");
        htmlConteudo.append("background-color:#D3D3D3;");
        htmlConteudo.append("}");
        htmlConteudo.append(".table-descontos th, .table-descontos td{");
        htmlConteudo.append("border: 1px solid;");
        htmlConteudo.append("padding: 4px;");
        htmlConteudo.append("}");

        htmlConteudo.append("</style>");
        htmlConteudo.append("</head>");
        htmlConteudo.append("<body>");
        htmlConteudo.append(StringUtils.isEmpty(html) ? "" : html);
        htmlConteudo.append("</body>");
        htmlConteudo.append("</html>");

        String espaco = "<span class=\"espaco\"></span>";
        String tab = espaco + espaco + espaco + espaco;


        Util.replaceAll(htmlConteudo, "\t", tab);
        Util.replaceAll(htmlConteudo, "&nbsp;", espaco);
        Util.replaceAll(htmlConteudo, "<p", "<div");
        Util.replaceAll(htmlConteudo, "</p>", "</div>");
        Util.replaceAll(htmlConteudo, "<br>", "<br/>");
        Util.replaceAll(htmlConteudo, "&amp;", E_COMERCIAL); // é necessário alterar todos os &amp; para E_COMERCIAL se não a próxima linha cai em um loop infinito
        Util.replaceAll(htmlConteudo, "&", E_COMERCIAL);
        Util.replaceAll(htmlConteudo, E_COMERCIAL, "&amp;");

        return htmlConteudo.toString();
    }


}