package br.com.kayros.service.controleacesso;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.persistencia.DAO;
import br.com.kayros.arquitetura.service.impl.ServiceImpl;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.dao.controleacesso.PerfilUsuarioDAO;
import br.com.kayros.enums.controleacesso.PerfilUsuarioStatus;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import br.com.kayros.service.controleacesso.menuacao.PerfilUsuarioMenuAcaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerfilUsuarioService extends ServiceImpl<PerfilUsuario> {

    public static final String INATIVAR_PERFIL_COM_USUARIO_VINCULADO = "perfilusuario.regraNegocio.naoPodeInativarComUsuarioVinculado";

    public static final String EXCLUIR_PERFIL_COM_USUARIO_VINCULADO = "perfilusuario.regraNegocio.naoPodeExcluirComUsuarioVinculado";

    @Autowired
    private PerfilUsuarioDAO perfilUsuarioDAO;

    @Autowired
    private PerfilUsuarioMenuAcaoService perfilUsuarioMenuAcaoService;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    protected DAO<PerfilUsuario> getDAO() {
        return perfilUsuarioDAO;
    }

    @Override
    public List<MenuAcao> getAcoes(PerfilUsuario perfilUsuario, Pagina pagina) {

        return perfilUsuarioMenuAcaoService.getAcoes(perfilUsuario, pagina);
    }

    @Override
    public void excluir(Long id) throws RegraNegocioException {
        isUsuarioVinculadoPerfil(perfilUsuarioDAO.get(id), EXCLUIR_PERFIL_COM_USUARIO_VINCULADO);
        getDAO().excluir(id);
    }

    public void ativar(PerfilUsuario perfilUsuario) throws RegraNegocioException {
        perfilUsuario.setPerfilUsuarioStatus(PerfilUsuarioStatus.ATIVO);
        alterar(perfilUsuario);
    }

    public void inativar(PerfilUsuario perfilUsuario) throws RegraNegocioException {
        isUsuarioVinculadoPerfil(perfilUsuario, INATIVAR_PERFIL_COM_USUARIO_VINCULADO);

        perfilUsuario.setPerfilUsuarioStatus(PerfilUsuarioStatus.INATIVO);
        alterar(perfilUsuario);
    }

    private void isUsuarioVinculadoPerfil(PerfilUsuario perfilUsuario, String mensagem) throws RegraNegocioException {
        if (!usuarioService.buscarUsuarioPorPerfil(perfilUsuario)) {
            RegraNegocioException.build(MensagemI18N.getKey(mensagem)).lancar();
        }
    }

    @Override
    public void salvar(PerfilUsuario entidade) throws RegraNegocioException {
        this.validaUnicidade(entidade);
        super.salvar(entidade);
    }


    @Override
    public void alterar(PerfilUsuario entidade) throws RegraNegocioException {
        this.validaUnicidade(entidade);
        super.alterar(entidade);
    }

    @Override
    public List<MenuAcao> getAcoesTabelaListagem(PerfilUsuario entidade) {
        return perfilUsuarioMenuAcaoService.getAcoesTabelaTelaListagem(entidade);
    }


    private void validaUnicidade(PerfilUsuario perfilUsuario) throws RegraNegocioException {
        PerfilUsuario perfilCadastrado = this.perfilUsuarioDAO.get("descricao", perfilUsuario.getDescricao());
        if (perfilCadastrado != null && perfilCadastrado.isAtivo() && (perfilUsuario.isNew() && perfilUsuario.getDescricao().equals(perfilCadastrado.getDescricao()) || (!perfilUsuario.isNew() && perfilUsuario.getDescricao().equals(perfilCadastrado.getDescricao()) && !perfilUsuario.getId().equals(perfilCadastrado.getId()))))
            throw new RegraNegocioException("Já existe um perfil cadastrado com essa descrição.");

    }
}
