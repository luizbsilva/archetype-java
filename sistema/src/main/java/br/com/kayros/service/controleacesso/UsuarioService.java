package br.com.kayros.service.controleacesso;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.enums.Status;
import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.persistencia.DAO;
import br.com.kayros.arquitetura.service.impl.ServiceImpl;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.arquitetura.util.Util;
import br.com.kayros.dao.controleacesso.UsuarioDAO;
import br.com.kayros.dto.controleacesso.UsuarioDTO;
import br.com.kayros.email.usuario.EmailUsuarioNovoUsuario;
import br.com.kayros.email.usuario.EmailUsuarioRecuperacaoSenha;
import br.com.kayros.modelo.controleacesso.PerfilUsuario;
import br.com.kayros.modelo.controleacesso.Usuario;
import br.com.kayros.service.comum.EmailSenderServiceImpl;
import br.com.kayros.service.controleacesso.menuacao.UsuarioMenuAcaoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class UsuarioService extends ServiceImpl<Usuario> {

    public static final String AUTENTIDACAO_AD = "autentidacaoAD";

    private static final String LOGIN = "login";

    public static final String SENHA = "senha";

    public static final String USUARIO_STAUS = "usuarioStatus";

    public static final String STATUS_USUARIO = "status";

    public static final String PRIMEIRO_ACESSO = "primeiroAcesso";

    @Autowired
    private Environment springEnv;

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Autowired
    private UsuarioMenuAcaoService usuarioMenuAcaoService;

    @Autowired
    private EmailSenderServiceImpl emailSenderService;

    @Autowired
    private AcessoService acessoService;

    @Override
    protected DAO<Usuario> getDAO() {
        return usuarioDAO;
    }

    @Override
    public List<MenuAcao> getAcoes(Usuario entidade, Pagina pagina) {
        return usuarioMenuAcaoService.getAcoes(entidade, pagina);
    }

    @Override
    public List<MenuAcao> getAcoesTabelaListagem(Usuario entidade) {
        return this.usuarioMenuAcaoService.getAcoesTabelaTelaListagem(entidade);
    }

    public void ativar(Usuario usuario) throws RegraNegocioException {
        usuario.setUsuarioStatus(Status.ATIVO);
        alterar(usuario);
    }

    public void inativar(Usuario usuario) throws RegraNegocioException {
        beforeInativar(usuario);
        usuario.setUsuarioStatus(Status.INATIVO);
        alterar(usuario);
    }

    @Override
    public void salvar(Usuario entidade) throws RegraNegocioException {
        this.validaUnicidade(entidade);
        String senha = Util.gerarSenha(8);
        entidade.setSenhaAntesDeEncriptar(senha);
        entidade.setSenha(Util.encriptSHA256(senha));

        super.salvar(entidade);

        emailSenderService.sendEmail(new EmailUsuarioNovoUsuario(entidade));
    }

    @Override
    public void alterar(Usuario entidade) throws RegraNegocioException {
        this.validaUnicidade(entidade);
        super.alterar(entidade);
    }

    private void validaUnicidade(Usuario usuario) throws RegraNegocioException {
        Usuario usuarioCadastrado = this.usuarioDAO.get(LOGIN, usuario.getLogin());
        if (usuarioCadastrado != null && usuarioCadastrado.isAtivo() && (usuario.isNew() && usuario.getLogin().equals(usuarioCadastrado.getLogin()) || (!usuario.isNew() && usuario.getLogin().equals(usuarioCadastrado.getLogin()) && !usuario.getId().equals(usuarioCadastrado.getId()))))
            throw new RegraNegocioException("Já existe um usuário cadastrado com esse login.");
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void beforeInativar(Usuario usuario) throws RegraNegocioException {

        if (usuario.getAdmin()) {
            RegraNegocioException.build(MensagemI18N.getKey("usuario.regraNegocio.naoPodeInativarAdmin")).lancar();
        }
    }

    public Usuario processarLogin(String username, String password) {

        Usuario usuario = usuarioDAO.get((Specification<Usuario>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.equal(root.<String>get(LOGIN), username),
                criteriaBuilder.equal(root.<Status>get(USUARIO_STAUS), Status.ATIVO),
                criteriaBuilder.equal(root.<Status>get(STATUS_USUARIO), Status.ATIVO)));
        return usuarioDAO.processarLogin(username, password);
    }

    public void confirmarDadosAutenticacao(String login, String senha) throws RegraNegocioException {

        usuarioDAO.confirmarDadosAutenticacao(login, senha);
    }

    public Usuario getLoginView(String login) {

        Usuario loginView = usuarioDAO.consultarLogin(login, null);
        if (!Objects.isNull(loginView)) {
            loginView.setSenha(null);
        }
        return loginView;
    }

    public void recuperarSenha(UsuarioDTO dto) {
        Usuario usuario = usuarioDAO.get((Specification<Usuario>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(LOGIN), dto.getLogin()), criteriaBuilder.equal(root.<Status>get(USUARIO_STAUS), Status.ATIVO), criteriaBuilder.equal(root.<Status>get(STATUS_USUARIO), Status.ATIVO)));

        String senha = Util.gerarSenha(8);
        usuario.setSenhaAntesDeEncriptar(senha);
        usuario.setSenha(Util.encriptSHA256(senha));
        usuario.setPrimeiroAcesso(Boolean.TRUE);
        usuarioDAO.alterarAtributos(usuario, SENHA, PRIMEIRO_ACESSO);

        emailSenderService.sendEmail(new EmailUsuarioRecuperacaoSenha(usuario));
    }

    public UsuarioDTO getByLogin(String login) {
        UsuarioDTO dto = new UsuarioDTO();
        Usuario usuario = usuarioDAO.get((Specification<Usuario>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(LOGIN), login), criteriaBuilder.not(root.get(AUTENTIDACAO_AD)), criteriaBuilder.equal(root.<Status>get(USUARIO_STAUS), Status.ATIVO), criteriaBuilder.equal(root.<Status>get(STATUS_USUARIO), Status.ATIVO)));

        if (Objects.isNull(usuario)) {
            return dto;
        }

        dto.setLogin(usuario.getLogin());
        dto.setEmail(usuario.getEmail());

        return dto;
    }

    public Date buscarUltimoAcesso(String login) {
        Usuario usuario = usuarioDAO.get((Specification<Usuario>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(LOGIN), login), criteriaBuilder.not(root.get(AUTENTIDACAO_AD)), criteriaBuilder.equal(root.<Status>get(USUARIO_STAUS), Status.ATIVO), criteriaBuilder.equal(root.<Status>get(STATUS_USUARIO), Status.ATIVO)));

        if (Util.isNullEmpty(usuario)) {
            return new Date();
        }

        Date acessoUltimoAcesso = acessoService.ultimoAcesso(usuario.getId());

        if (Util.isNullEmpty(acessoUltimoAcesso)) {
            return new Date();
        }

        return acessoUltimoAcesso;
    }

    public void salvarNovaSenha(Usuario usuario) {
        usuarioDAO.salvarNovaSenha(usuario);
    }

    public Boolean buscarUsuarioPorPerfil(PerfilUsuario perfilUsuario) {
        return usuarioDAO.buscarUsuarioPorPerfil(perfilUsuario);
    }
}
