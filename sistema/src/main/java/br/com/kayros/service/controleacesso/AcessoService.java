package br.com.kayros.service.controleacesso;

import br.com.kayros.arquitetura.persistencia.DAO;
import br.com.kayros.arquitetura.service.impl.ServiceImpl;
import br.com.kayros.arquitetura.util.DateUtil;
import br.com.kayros.dao.controleacesso.AcessoDAO;
import br.com.kayros.modelo.controleacesso.Acesso;
import br.com.kayros.modelo.controleacesso.Usuario;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Service
public class AcessoService extends ServiceImpl<Acesso> {

    private static Logger logger = Logger.getLogger(AcessoService.class.getSimpleName());

    @Autowired
    private AcessoDAO acessoDAO;

    @Override
    protected DAO<Acesso> getDAO() {
        return acessoDAO;
    }

    public void registrarAcesso(Usuario usuario, String id) {
        if (!Objects.isNull(usuario)) {
            Acesso acesso = new Acesso();
            acesso.setUsuario(usuario);
            acesso.setIp(id);

            String usuarioData = usuario.getIdentifierName() + " em " + DateUtil.formataDataHora(new Date());

            try {
                salvar(acesso);
                logger.info("Acesso registrado para o usuário " + usuarioData);

            } catch (Exception e) {
                logger.warn("Não foi possível registrar o acesso do usuário " + usuarioData, e);

            }
        }
    }

    public Long countPorUsuario(Long usuarioId) {
        return acessoDAO.countPorUsuario(usuarioId);
    }

    public Date ultimoAcesso(Long usuarioId) {
        return acessoDAO.ultimoAcessoUsuario(usuarioId);
    }
}
