package br.com.kayros.service.controleacesso.menuacao;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifierContext;
import br.com.kayros.arquitetura.service.impl.MenuAcaoServiceImpl;
import br.com.kayros.modelo.controleacesso.Usuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioMenuAcaoService extends MenuAcaoServiceImpl<Usuario> {

    @Override
    public Modulo getModulo() {

        return Modulo.USUARIO;
    }

    @Override
    protected void adicionarAcoesPaginaListagem(List<MenuAcao> acoes) {

        acoes.add(getAcaoNovo());
    }

    public List<MenuAcao> getAcoesTabelaTelaListagem(Usuario usuario) {

        List<MenuAcao> acoes = new ArrayList<>();
        AuthIdentifier usuarioLogado = AuthIdentifierContext.getCurrentAuthIdentifier();


        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.ATIVAR_INATIVAR) && !usuarioLogado.getIdentifier().equals(usuario.getId())) {
            usuario.getUsuarioStatus().ifAtivo(() -> acoes.add(getAcaoInativarTabelaListagem()))
                    .ifInativo(() -> acoes.add(getAcaoAtivarTabelaListagem()));
        }

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.CONSULTAR) || contemPermissao(Funcionalidade.CADASTRAR)) {
            acoes.add(super.getAcaoConsultarTabelaListagem());
        }

        if (validarPermissao(usuarioLogado.getAdmin(), Funcionalidade.EDITAR)) {
            acoes.add(super.getAcaoEditarTabelaListagem());
        }

        return acoes;
    }
}
