CREATE TABLE kayros_mais.tb_perfil_usuario
(
    id bigserial PRIMARY KEY NOT NULL,
    status int NOT NULL,
    atualizado_em timestamp,
    atualizado_por varchar(200),
    criado_em timestamp NOT NULL,
    criado_por varchar(100) NOT NULL,
    descricao varchar(255) NOT NULL,
    perfil_usuario_status varchar(100)
);
CREATE UNIQUE INDEX tb_perfil_usuario_id_uindex ON kayros_mais.tb_perfil_usuario (id);

CREATE TABLE kayros_mais.tb_perfil_usuario_modulo
(
    id bigserial PRIMARY KEY NOT NULL,
    status int NOT NULL,
    modulo varchar(255),
    id_perfil_usuario bigint NOT NULL,
    CONSTRAINT fk_modulo_por_perfil FOREIGN KEY (id_perfil_usuario) REFERENCES kayros_mais.tb_perfil_usuario (id)
);
CREATE UNIQUE INDEX tb_perfil_usuario_modulo_id_uindex ON kayros_mais.tb_perfil_usuario_modulo (id);

CREATE TABLE kayros_mais.tb_usuario
(
    id bigserial PRIMARY KEY NOT NULL,
    status int NOT NULL,
    atualizado_em timestamp,
    atualizado_por varchar(100),
    criado_em timestamp NOT NULL,
    criado_por varchar(100) NOT NULL,
    admin boolean,
    cpf varchar(100),
    data_nascimento date,
    email varchar(100) NOT NULL,
    login varchar(100) NOT NULL,
    nome varchar(200) NOT NULL,
    primeiro_acesso boolean DEFAULT true  NOT NULL,
    senha varchar(255) NOT NULL,
    setor varchar(100),
    telefone varchar(50),
    usuario_status varchar(100),
    id_perfil_usuario bigint,
    CONSTRAINT fk_usuario_perfil FOREIGN KEY (id_perfil_usuario) REFERENCES kayros_mais.tb_perfil_usuario (id)
);
CREATE UNIQUE INDEX tb_usuario_id_uindex ON kayros_mais.tb_usuario (id);


CREATE TABLE kayros_mais.tb_acesso
(
    id bigserial PRIMARY KEY NOT NULL,
    status int NOT NULL,
    data_hora timestamp NOT NULL,
    horario_acesso date,
    ip_acesso varchar(255),
    id_usuario bigint NOT NULL,
    CONSTRAINT fk_acesso_usuario FOREIGN KEY (id_usuario) REFERENCES kayros_mais.tb_usuario (id)
);
CREATE UNIQUE INDEX tb_acesso_id_uindex ON kayros_mais.tb_acesso (id);