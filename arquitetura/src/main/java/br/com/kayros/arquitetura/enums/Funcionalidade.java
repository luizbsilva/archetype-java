package br.com.kayros.arquitetura.enums;

import java.util.HashSet;
import java.util.Set;


public enum Funcionalidade {
    CONSULTAR,
    CADASTRAR,
    EDITAR,
    EXCLUIR,
    ATIVAR_INATIVAR,
    GERAR_NOVA_VERSAO,
    CANCELAR,
    IMPORTAR,
    ENVIAR,
    RETOMAR;

    Funcionalidade() {
    }

    public Set<Funcionalidade> getFuncionalidadesDependentes() {
        Set<Funcionalidade> funcionalidades = new HashSet<>();

        switch (this) {
            case CADASTRAR:
                funcionalidades.add(CONSULTAR);
                break;
            case EDITAR:
                funcionalidades.add(CONSULTAR);
                break;
            case EXCLUIR:
                funcionalidades.add(CONSULTAR);
                break;
            case ATIVAR_INATIVAR:
                funcionalidades.add(CONSULTAR);
                break;
            case GERAR_NOVA_VERSAO:
                funcionalidades.add(CONSULTAR);
                break;
            case CANCELAR:
                funcionalidades.add(CONSULTAR);
                break;
            case IMPORTAR:
                funcionalidades.add(CONSULTAR);
                break;
        }

        return funcionalidades;
    }
}
