package br.com.kayros.arquitetura.util;

public interface MediaType {

    String APPLICATION_PDF = "application/pdf";
    String APPLICATION_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


}
