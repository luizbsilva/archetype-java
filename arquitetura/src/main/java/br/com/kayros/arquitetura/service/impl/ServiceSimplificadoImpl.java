package br.com.kayros.arquitetura.service.impl;

import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.persistencia.impl.DAOSimplificadoImpl;
import br.com.kayros.arquitetura.service.Service;
import br.com.kayros.arquitetura.util.RegraNegocioException;

public abstract class ServiceSimplificadoImpl<E extends ObjectID> extends ServiceConsultarImpl<E> implements Service<E> {

    @Override
    public void salvar(E entidade) throws RegraNegocioException {
        getDAO().salvar(entidade);
    }

    @Override
    public void alterar(E entidade) throws RegraNegocioException {
        getDAO().alterar(entidade);
    }

    @Override
    public void excluir(Long id) {
        getDAO().excluir(id);
    }

    @Override
    public void excluirDefinitivamente(Long id) {
        getDAO().excluirDefinitivamente(id);
    }

    protected abstract DAOSimplificadoImpl<E> getDAO();
}