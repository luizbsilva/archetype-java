package br.com.kayros.arquitetura.modelo;

import java.util.Date;

public interface EntidadeAuditavel extends Entidade {

    String getCriadoPor();

    void setCriadoPor(String criadoPor);

    Date getCriadoEm();

    void setCriadoEm(Date criadoEm);

    String getAtualizadoPor();

    void setAtualizadoPor(String atualizadoPor);

    Date getAtualizadoEm();

    void setAtualizadoEm(Date atualizadoEm);
}
