package br.com.kayros.arquitetura.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.util.internal.StringUtil;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.slf4j.Logger;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.text.MaskFormatter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

    public static String completarZeroAEsquerda(
            int completar,
            String valor
    ) {

        if (StringUtils.isEmpty(valor))
            return null;

        int size = valor.length();

        int total = completar - size;

        String zero = "";

        for (int i = 0; i < total; i++) {

            zero = zero + 0;

        }

        return (zero + valor).toString();
    }

    public static boolean valorUmEstaEntreValorDois(String valorUm, String valorDois) {

        return valorUm.indexOf(valorDois) >= 0;
    }

    public static void toUppercaseAllStringFields(Object obj) {
        FieldUtils.getAllFieldsList(obj.getClass()).stream()
                .filter(f -> f.getType().isAssignableFrom(String.class))
                .forEach(f -> {
                    try {
                        String value = (String) FieldUtils.readField(f, obj, true);
                        if (!StringUtils.isEmpty(value)) {
                            FieldUtils.writeField(f, obj, value.toUpperCase());
                        }
                    } catch (IllegalAccessException ignored) {
                    }
                });
    }

    public static Type[] getTypes(Object object) {
        ParameterizedType parameterizedType;

        try {
            parameterizedType = (ParameterizedType) object.getClass().getGenericSuperclass();

        } catch (ClassCastException e) {
            try {
                parameterizedType = (ParameterizedType) Class.forName(object.getClass().getGenericSuperclass().getTypeName()).getGenericSuperclass();

            } catch (ClassNotFoundException classNotFountexception) {
                throw new RuntimeException(classNotFountexception);
            }

        }

        return parameterizedType.getActualTypeArguments();
    }

    public static String encriptSHA256(String valor) {
        return encript(valor, "SHA-256");
    }

    public static String encript(String valor, String algorithm) {
        return Hex.encodeHexString(DigestUtils.getDigest(algorithm).digest(valor.getBytes()));
    }

    public static <T> T clonar(T objeto) {
        T clone = null;

        try {
            clone = (T) objeto.getClass().newInstance();

            for (final Field field : FieldUtils.getAllFields(objeto.getClass())) {
                if (!ignorarFieldDuranteClonagem(field)) {
                    field.setAccessible(true);
                    field.set(clone, field.get(objeto));
                }
            }
        } catch (Exception ignored) {
        }

        return clone;
    }

    private static boolean ignorarFieldDuranteClonagem(Field field) {
        boolean ignorarField = false;

        List<Class<? extends Annotation>> anotationsIgnoradas = new ArrayList<>();
        anotationsIgnoradas.add(Id.class);
        anotationsIgnoradas.add(CreationTimestamp.class);
        anotationsIgnoradas.add(UpdateTimestamp.class);

        for (Class<? extends Annotation> anotationsIgnorada : anotationsIgnoradas) {
            if (field.isAnnotationPresent(anotationsIgnorada)) {
                ignorarField = true;
                break;
            }
        }

        if (!ignorarField && field.isAnnotationPresent(Column.class)) {
            List<String> colunasIgnoradas = new ArrayList<>();
            colunasIgnoradas.add("criado_por");
            colunasIgnoradas.add("atualizado_por");

            for (String colunasIgnorada : colunasIgnoradas) {
                if (colunasIgnorada.equals(field.getAnnotation(Column.class).name())) {
                    ignorarField = true;
                    break;
                }
            }
        }

        if (Collection.class.isAssignableFrom(field.getType())) {
            ignorarField = true;
        }

        return ignorarField;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getValorFromField(Object entidade, Field field) {
        T valor = null;

        try {
            valor = (T) FieldUtils.readField(field, entidade, true);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return valor;
    }

    public static void replaceAll(StringBuilder value, String oldValue, String newValue) {
        int indexOf = value.indexOf(oldValue);
        while (indexOf >= 0) {
            value.replace(indexOf, indexOf + oldValue.length(), newValue);
            indexOf = value.indexOf(oldValue);
        }
    }

    public static String replaceAll(String value, String oldValue, String newValue) {
        StringBuilder valueTemp = new StringBuilder(value);

        replaceAll(valueTemp, oldValue, newValue);

        return valueTemp.toString();
    }

    public static String gerarSenha(int qtdeCaracteres) {
        StringBuilder senha = new StringBuilder();

        while (qtdeCaracteres > 0) {
            int numeroSenha = (int) (Math.random() * 10);
            senha.append(numeroSenha);
            qtdeCaracteres--;
        }

        return senha.toString();
    }

    public static Set<String> getTodasTagsUtilizadas(String texto) {

        Set<String> tags = new HashSet<>();

        Pattern pattern = Pattern.compile("(\\$)(\\{.*?\\})");

        Matcher matcher = pattern.matcher(texto);

        while (matcher.find()) {

            tags.add(texto.substring(matcher.start(), matcher.end()));
        }

        return tags;
    }

    public static Double resolucaoDeFormula(String formula, Map<String, Number> parametros) throws Exception {
        formula = formula.replace("${", "").replace("}", "").replace(",", ".");

        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("javascript");

        Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
        parametros.entrySet().forEach(entry -> bindings.put(entry.getKey().replace("${", "").replace("}", ""), entry.getValue()));

        Number valor = (Number) scriptEngine.eval(formula);

        return valor.doubleValue();
    }

    public static String formatarValor(Object object) {
        return formatarValor(object, 2);
    }

    public static String formatarValor(Object object, Integer mininumFractionDigits) {
        String valor = "";

        if (Objects.nonNull(object)) {
            NumberFormat numberFormat = NumberFormat.getNumberInstance(DateUtil.localeDefault);
            numberFormat.setMinimumFractionDigits(mininumFractionDigits);
            numberFormat.setMaximumFractionDigits(mininumFractionDigits);
            valor = numberFormat.format(object);
        }

        return valor;
    }

    public static String formatarValorComDuasCasasDecimais(Object object) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(DateUtil.localeDefault);
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);
        return numberFormat.format(object);
    }

    public static void copiarAtributos(Object de, Object para) {
        Set<String> namesFieldsPara = FieldUtils.getAllFieldsList(para.getClass()).stream().map(Field::getName).collect(Collectors.toSet());

        FieldUtils.getAllFieldsList(de.getClass()).forEach(fieldDe -> {
            try {
                if (namesFieldsPara.contains(fieldDe.getName())) {
                    fieldDe.setAccessible(true);

                    Field fieldPara = FieldUtils.getField(para.getClass(), fieldDe.getName(), true);
                    fieldPara.setAccessible(true);
                    fieldPara.set(para, fieldDe.get(de));
                }
            } catch (Exception ignored) {
            }
        });
    }

    /**
     * @param byteArray um byte[]
     * @return O Tamanho de uma imagem [width, height]
     */
    public static int[] getImageHeight(byte[] byteArray) {
        int width;
        int height;

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray)) {
            BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
            width = bufferedImage.getWidth();
            height = bufferedImage.getHeight();

        } catch (Exception e) {
            width = 0;
            height = 0;

        }

        return new int[]{width, height};
    }

    public static File createFile(String fileBase64, String fileName) {
        return createFile(Base64.getDecoder().decode(fileBase64), fileName);
    }

    public static File createFile(byte[] fileBytesArray, String fileName) {
        File file = new File(FileUtils.getTempDirectory().getPath() + File.separator + fileName);

        try {
            FileUtils.writeByteArrayToFile(file, fileBytesArray);
        } catch (IOException ignored) {
        }

        return file;
    }

    public static String replaceCaminhoRelativoPorAbsoluto(String texto, String urlCaminhoAbsoluto) {
        if (!StringUtils.isEmpty(texto) && !StringUtils.isEmpty(urlCaminhoAbsoluto)) {
            urlCaminhoAbsoluto = urlCaminhoAbsoluto.endsWith("/") ? urlCaminhoAbsoluto : urlCaminhoAbsoluto + "/";

            String stringRegex = "(\\.\\./)+";

            Pattern pattern = Pattern.compile(stringRegex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
            Matcher matcher = pattern.matcher(texto);
            if (matcher.find()) {
                texto = texto.replaceAll(stringRegex, urlCaminhoAbsoluto);
            }
        }

        return texto;
    }

    public static String replaceCaracteresAcentuadosPorCaracteresSemAcentos(String valor) {
        return Normalizer.normalize(valor, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    public static boolean isNull(Object object, String atributo) {
        return Objects.isNull(getAtributo(object, atributo));
    }

    public static <T> T getAtributo(Object object, String atributo) {
        String[] atributos = atributo.split("\\.");

        Object objectTemp = object;
        for (String atributoTemp : atributos) {
            try {
                objectTemp = FieldUtils.readField(objectTemp, atributoTemp, true);

            } catch (Exception e) {
                objectTemp = null;
                break;
            }
        }

        return (T) objectTemp;
    }

    public static void imprimirLogJson(Logger logger, String descricao, Object object) {
        try {
            logger.info(descricao);
            logger.info("JSON: " + new ObjectMapper().writeValueAsString(object));

        } catch (Exception e) {
            logger.error("Não foi possível imprimir o Log de JSON (" + descricao + ")", e);

        }
    }

    public static String formatarCpf(String cpf) {
        String newCpf;

        try {
            MaskFormatter mask = new MaskFormatter("###.###.###-##");
            mask.setValueContainsLiteralCharacters(false);
            newCpf = mask.valueToString(cpf);
        } catch (ParseException ignored) {
            newCpf = cpf;
        }

        return newCpf;
    }

    public static boolean isNullEmpty(final Object object) {
        if (object == null) {
            return true;
        } else if (object instanceof String && StringUtil.isNullOrEmpty(object.toString())) {
            return true;
        } else if (object instanceof Collection && ((Collection<?>) object).isEmpty()) {
            return true;
        } else {
            return object instanceof Map && ((Map<?, ?>) object).isEmpty();
        }
    }

    public static String getValorOuVazio(final Object object) {
        if (isNotNullEmpty(object)) {
            return object.toString();
        }
        return isNotNullEmpty(object) ? object.toString() : "";
    }

    public static boolean isNotNullEmpty(final Object object) {
        return !isNullEmpty(object);
    }
}
