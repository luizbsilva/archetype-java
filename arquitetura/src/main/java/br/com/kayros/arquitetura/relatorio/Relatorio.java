package br.com.kayros.arquitetura.relatorio;

public interface Relatorio<T, FILTRO> {

    T gerarRelatorio(FILTRO filtro);

}
