package br.com.kayros.arquitetura.util;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder {

    private Map<String, Object> map = new HashMap<>();

    public static MapBuilder create() {
        return new MapBuilder();
    }

    public static MapBuilder create(String key, Object value) {
        MapBuilder builder = create();

        builder.map.put(key, value);

        return builder;
    }

    public MapBuilder map(String key, Object value) {

        this.map.put(key, value);

        return this;
    }

    public Map<String, Object> build() {

        return this.map;
    }
}
