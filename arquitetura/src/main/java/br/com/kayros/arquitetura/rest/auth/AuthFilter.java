package br.com.kayros.arquitetura.rest.auth;

import br.com.kayros.arquitetura.service.impl.AuthService;
import br.com.kayros.arquitetura.util.Util;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class AuthFilter<A extends AuthIdentifier> implements ContainerRequestFilter {

    @Autowired
    private AuthService authService;

    @Context
    protected HttpServletRequest request;

    @Context
    protected ResourceInfo info;

    public static final String AUTHORIZATION = "Authorization";

    public void filter(final ContainerRequestContext containerRequestContext) throws IOException {

        if (!containerRequestContext.getMethod().equalsIgnoreCase("OPTIONS")) {
            if (!this.anotacaoEstaPresente(PermitAll.class)) {
                validarUsuarioLogado(containerRequestContext);

            }
        }
    }

    protected boolean anotacaoEstaPresente(Class<? extends Annotation> annotationClass) {
        return info.getResourceClass().isAnnotationPresent(annotationClass) || info.getResourceMethod().isAnnotationPresent(annotationClass);
    }

    private <T extends Annotation> T obterAnotacao(Class<T> annotationClass) {
        return (this.info.getResourceMethod().isAnnotationPresent(annotationClass)) ? info.getResourceMethod().getAnnotation(annotationClass) : info.getResourceClass().getAnnotation(annotationClass);
    }

    private void validarUsuarioLogado(final ContainerRequestContext containerRequestContext) throws IOException {
        if (this.anotacaoEstaPresente(DenyAll.class)) {
            throw new NotAuthorizedException("Nobody can access this resource");
        }

        final String token = containerRequestContext.getHeaderString(AUTHORIZATION);

        if (!authService.validarToken(token)) {
            throw new NotAuthorizedException("Access denied for this resource");
        }

        A authIdentifier = authService.getAuthIdentifierFromToken(token, (Class<A>) Util.getTypes(this)[0]);

        if (this.anotacaoEstaPresente(RolesAllowed.class)) {
            final Set<String> roles = new HashSet<>(Arrays.asList(obterAnotacao(RolesAllowed.class).value()));

            if (!isUserAllowed(authIdentifier, roles)) {
                throw new NotAuthorizedException("Access denied for this resource");
            }
        }

        AuthIdentifierContext.setCurrentAuthIdentifier(authIdentifier);
    }

    protected abstract boolean isUserAllowed(A authIdentifier, Set<String> rolesSet);
}
