package br.com.kayros.arquitetura.email;


import br.com.kayros.arquitetura.enums.ConfiguracaoEmailProtocolo;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public interface EmailSenderConfiguration {

    String getHost();

    String getPorta();

    String getRemetente();

    String getUsuario();

    String getSenha();

    String getUrlSistema();

    ConfiguracaoEmailProtocolo getProtocolo();

    default boolean possuiTodasInformacoess() {
        return !StringUtils.isEmpty(getHost())
                && !StringUtils.isEmpty(getRemetente())
                && !StringUtils.isEmpty(getUsuario())
                && !StringUtils.isEmpty(getSenha())
                && !Objects.isNull(getProtocolo());
    }

    default boolean isProtocoloSSL() {
        return ConfiguracaoEmailProtocolo.SSL == getProtocolo();
    }

}
