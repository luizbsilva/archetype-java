package br.com.kayros.arquitetura.enums;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Modulo {

    USUARIO(Sistema.CONTROLE_ACESSO),
    PERFIL_USUARIO(Sistema.CONTROLE_ACESSO);

    @NonNull
    private Sistema sistema;
}
