package br.com.kayros.arquitetura.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@JsonIgnoreProperties({"message", "suppressed", "localizedMessage", "cause", "stackTrace"})
@Getter
public class RegraNegocioException extends Exception {

    private List<String> mensagens;

    public static RegraNegocioException build() {
        return new RegraNegocioException();
    }

    public static RegraNegocioException build(String mensagem) {
        return build().addMensagem(mensagem);
    }

    public static RegraNegocioException build(Collection<String> mensagens) {
        return build().addMensagens(mensagens);
    }

    public RegraNegocioException() {
        super("Regra Negocio Exception");
        mensagens = new ArrayList<>();
    }

    public RegraNegocioException(String mensagem) {
        this();
        addMensagem(mensagem);
    }

    public RegraNegocioException addMensagem(String mensagem) {
        mensagens.add(mensagem);
        return this;
    }

    public RegraNegocioException addMensagens(Collection<String> mensagens) {
        this.mensagens.addAll(mensagens);
        return this;
    }

    public String getMensagensSeparadosPorLinhas() {
        return mensagens.stream().reduce("", (valorReduzido, valor) -> valorReduzido + valor + "\n");
    }

    public void lancar() throws RegraNegocioException {
        if (!mensagens.isEmpty()) {
            throw this;
        }
    }
}
