package br.com.kayros.arquitetura.modelo.impl;

import br.com.kayros.arquitetura.modelo.EntidadeAuditavel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class EntidadeAuditavelImpl extends EntidadeImpl implements EntidadeAuditavel {

    @Column(name = "criado_por", updatable = false)
    private String criadoPor;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name = "criado_em", updatable = false)
    private Date criadoEm;

    @Column(name = "atualizado_por")
    private String atualizadoPor;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = "atualizado_em")
    private Date atualizadoEm;

}
