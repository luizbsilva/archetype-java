package br.com.kayros.arquitetura.persistencia;

import br.com.kayros.arquitetura.dto.PaginacaoResultado;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Selection;
import java.io.Serializable;
import java.util.List;

public interface DAOConsultar<O extends ObjectID> {

    O get(Long id);

    O get(String atributo, Object valor);

    O get(Specification<O> specification);

    List<O> listar();

    List<O> listar(Specification<O> specification);

    List<O> listar(List<Selection<?>> selections, Specification<O> specification);

    PaginacaoResultado<O> listar(Paginacao paginacao);

    <T> List<T> dtoListar();

    PaginacaoResultado<?> dtoListar(Paginacao paginacao);

    <D> PaginacaoResultado<D> dtoListar(Paginacao paginacao, Class<D> dtoClass);

    long contar(Paginacao paginacao);

    long contar(Paginacao paginacao, Class<?> dtoClass);

    boolean existe(Specification<O> specification);

    <T> T getValorAtributo(String atributo, Serializable id);

    <T> T getValorAtributo(String atributo, Specification<O> specification);
}
