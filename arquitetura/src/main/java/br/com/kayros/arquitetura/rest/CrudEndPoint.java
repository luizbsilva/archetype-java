package br.com.kayros.arquitetura.rest;

import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.service.Service;
import br.com.kayros.arquitetura.util.MapBuilder;
import br.com.kayros.arquitetura.util.RegraNegocioException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public abstract class CrudEndPoint<E extends ObjectID, ID extends Serializable> extends ConsultarEndPoint<E, ID> {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(E entidade) {
        return onExecuteService(entidade, () -> getServico().salvar(entidade));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterar(E entidade) {
        return onExecuteService(entidade, () -> getServico().alterar(entidade));
    }

    @SuppressWarnings("RestParamTypeInspection")
    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response excluir(@PathParam("id") Long id) {
        return onExecuteService(null, () -> getServico().excluir(id));
    }

    protected <T> Map<String, Object> getMapResposta(T entidade, String mensagem, String... parametros) {
        return getMapResposta(entidade, Objects.isNull(mensagem) ? Collections.emptyList() : Collections.singletonList(MensagemI18N.getKey(mensagem, parametros)));
    }

    protected <T> Map<String, Object> getMapResposta(T entidade, Collection<String> mensagens) {
        MapBuilder mapBuilder = MapBuilder.create();
        mapBuilder.map("entidade", entidade);
        mapBuilder.map("mensagens", mensagens);

        return mapBuilder.build();
    }


    public <T> Response onExecuteService(T entidade, OnExecuteServiceCallBack callBack) {
        Response response;

        try {
            callBack.executar();
            response = callBack.getResponse();

            if (Objects.isNull(response)) {
                response = Response.ok(getMapResposta(entidade, callBack.getMensagem())).build();
            }
        } catch (RegraNegocioException e) {
            response = gerarResponseDeRegraNegocioException(e);

        }

        return response;
    }

    protected abstract Service<E> getServico();

    protected interface OnExecuteServiceCallBack {
        void executar() throws RegraNegocioException;

        default Response getResponse() {
            return null;
        }

        default String getMensagem() {
            return "operacao.realizada.sucesso";
        }
    }
}
