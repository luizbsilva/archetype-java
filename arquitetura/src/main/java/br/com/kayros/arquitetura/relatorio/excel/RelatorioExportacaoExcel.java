package br.com.kayros.arquitetura.relatorio.excel;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;
import java.util.function.BiConsumer;

public abstract class RelatorioExportacaoExcel<T, FILTRO> extends RelatorioExcel<FILTRO> {

    protected void addConteudoRelatorio(XSSFSheet planilha, FILTRO filtro) {
        configurarRelatorio(planilha);

        preencherRelatorio(planilha, listar(filtro));
    }

    private void preencherRelatorio(XSSFSheet planilha, List<T> entidades) {
        if (entidades.isEmpty()) {
            addLinhaNenhumRegistroEncontrado(planilha);

        } else {
            entidades.forEach(entidade -> {
                int numeroLinha = getProximaLinhaDisponivel(planilha);
                onRowIterate(planilha).accept(numeroLinha, entidade);
            });

            addLinhaTotalizacao(planilha, entidades);
        }
    }


    protected abstract void configurarRelatorio(XSSFSheet planilha);

    protected abstract List<T> listar(FILTRO filtro);

    protected abstract BiConsumer<Integer, T> onRowIterate(XSSFSheet planilha);

}
