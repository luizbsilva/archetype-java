package br.com.kayros.arquitetura.email;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface Email {
    String getId();

    String getAssunto();

    String getTextoEmail();

    List<String> getDestinatario();

    default List<String> getCC() {
        return Collections.emptyList();
    }

    default List<String> getBCC() {
        return Collections.emptyList();
    }

    default Map<String, String> getParametros() {
        return new HashMap<>();
    }

    default List<File> getAnexos() {
        return Collections.emptyList();
    }
}
