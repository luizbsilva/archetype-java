package br.com.kayros.arquitetura.deserializer;

import br.com.kayros.arquitetura.modelo.Entidade;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public abstract class CustomDesesializer<E extends Entidade> extends JsonDeserializer<E> {

    private JsonNode jsonNode;

    @Override
    public E deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
        jsonNode = jsonParser.getCodec().readTree(jsonParser);

        return getDeserializedObject();
    }

    protected final boolean has(String fieldName) {
        return jsonNode.has(fieldName);
    }

    protected final Long getLong(String fieldName) {
        return has(fieldName) ? jsonNode.get(fieldName).asLong() : null;
    }

    protected abstract E getDeserializedObject();

}
