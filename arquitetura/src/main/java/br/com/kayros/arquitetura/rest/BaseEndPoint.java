package br.com.kayros.arquitetura.rest;

import br.com.kayros.arquitetura.util.Constantes;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.BufferedOutputStream;

public class BaseEndPoint {

    @Autowired
    private HttpServletRequest request;

    public Response gerarResponseDeRegraNegocioException(RegraNegocioException e) {
        return gerarResponseDeRegraNegocioException(Response.Status.BAD_REQUEST, e);
    }

    public Response gerarResponseDeRegraNegocioException(Response.Status status, RegraNegocioException e) {
        return Response.status(status).entity(e).type(MediaType.APPLICATION_JSON).build();
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public String getIpDeOrigem() {
        String xRealIp = request.getHeader(Constantes.HEADER_X_REAL_IP);

        return StringUtils.isEmpty(xRealIp) ? getRequest().getRemoteAddr() : xRealIp;
    }

    public Response downloadArquivo(String nome, byte[] bytes) {
        return downloadArquivo(nome, MediaType.APPLICATION_OCTET_STREAM, bytes);
    }

    public Response downloadArquivo(String nome, String type, byte[] bytes) {
        StreamingOutput streamingOutput = output -> {
            try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(output)) {
                bufferedOutputStream.write(bytes);
                bufferedOutputStream.flush();

            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        return downloadArquivo(nome, type, streamingOutput);
    }

    public Response downloadArquivo(String nome, String type, StreamingOutput streamingOutput) {
        return Response.ok(streamingOutput, type).header("Content-Disposition", "attachment;filename=\"" + nome + "\"").build();
    }

    public String retornarValorString(String valor) {
        String valorEncapsulado;

        try {
            valorEncapsulado = new ObjectMapper().writeValueAsString(valor);

        } catch (Exception e) {
            valorEncapsulado = null;
        }

        return valorEncapsulado;
    }
}
