package br.com.kayros.arquitetura.dto.impl;

import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public abstract class EntidadeAuditavelDTO<E> extends EntidadeDTO<E> {

    @ProjectionProperty("criadoPor")
    private String criadoPor;

    @ProjectionProperty("criadoEm")
    private Date criadoEm;

    @ProjectionProperty("atualizadoPor")
    private String atualizadoPor;

    @ProjectionProperty("atualizadoEm")
    private Date atualizadoEm;

}
