package br.com.kayros.arquitetura.util;


import br.com.kayros.arquitetura.enums.ConfiguracaoEmailProtocolo;

public interface Constantes {

    String SCHEMA_BANCO = "kayros_mais";

    String HEADER_X_REAL_IP = "X-Real-IP";

    String SPRING_PROFILE_DEV = "dev";
    String SPRING_PROFILE_QAS = "qas";
    String SPRING_PROFILE_QAS_8084 = "qas-8084";
    String SPRING_PROFILE_PRD = "prd";
    String SPRING_PROFILE_TESTE = "teste";

    String SMTP_SERVER_EMAIL = "smtp.office365.com";
    String PORTA_SERVER_EMAIL = "587";
    ConfiguracaoEmailProtocolo PROTOCOLO_EMAIL = ConfiguracaoEmailProtocolo.TLS;

}
