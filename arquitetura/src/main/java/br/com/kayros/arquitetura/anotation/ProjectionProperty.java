package br.com.kayros.arquitetura.anotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
public @interface ProjectionProperty {

    String value() default "";

    boolean sum() default false;

    boolean max() default false;

    boolean count() default false;
}
