package br.com.kayros.arquitetura.service;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Pagina;

import java.util.List;


public interface MenuAcaoService<E> {

    List<MenuAcao> getAcoes(E entidade, Pagina pagina);

    List<MenuAcao> getAcoesTabelaListagem(E entidade);

}
