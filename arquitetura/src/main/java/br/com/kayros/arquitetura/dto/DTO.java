package br.com.kayros.arquitetura.dto;

import java.io.Serializable;

public interface DTO<ID extends Serializable> {

    ID getId();

    void setId(ID id);

}
