package br.com.kayros.arquitetura.serializer.impl;

import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.serializer.CustomSerializer;

import java.io.IOException;

public class IDSerializer extends CustomSerializer<ObjectID<?>> {

    @Override
    protected void serializeAttributes(ObjectID entidade) throws IOException {

    }
}
