package br.com.kayros.arquitetura.relatorio.pdf;

import br.com.kayros.arquitetura.paginacao.Paginacao;
import br.com.kayros.arquitetura.relatorio.Relatorio;
import br.com.kayros.arquitetura.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.util.ResourceUtils;

import java.util.*;

@Slf4j
public abstract class RelatorioPdf<T, FILTRO> implements Relatorio<String, FILTRO> {

    @Override
    public String gerarRelatorio(FILTRO filtro) {
        byte[] relatorio;

        try {
            ajustarPaginacao(filtro);

            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listar(filtro));

            JasperPrint print = JasperFillManager.fillReport(getPathRelatorio(getJarterReport()), getParametros(filtro), dataSource);

            relatorio = JasperExportManager.exportReportToPdf(print);

        } catch (Exception e) {
            log.error("Não possível gerar o relatório.", e);
            relatorio = new byte[0];
        }

        return Base64.getEncoder().encodeToString(relatorio);
    }

    protected final String getPathRelatorio(String path) throws Exception {
        return ResourceUtils.getFile("classpath:" + path).getPath();
    }

    private Map<String, Object> getParametros(FILTRO filtro) throws Exception {
        Map<String, Object> parametros = new HashMap<>();

        addParametrosDefault(parametros);
        addParametros(parametros);

        return parametros;
    }

    private void addParametrosDefault(Map<String, Object> parametros) {
        parametros.put("dataHora", DateUtil.formataDataHora(new Date()));
        parametros.put("urlSistema", getUrlSistema());
        parametros.put("logoSistema", getLogoSistema());
    }

    private void ajustarPaginacao(FILTRO filtro) {
        if (filtro instanceof Paginacao) {
            Paginacao paginacao = (Paginacao) filtro;
            paginacao.setListarTodos(true);
            paginacao.setPreencherAcoes(false);
        }
    }

    protected String getLogoSistema() {
        return "/assets/imagens/logo.png";
    }

    protected void addParametros(Map<String, Object> parametros) throws Exception {
    }

    protected abstract List<T> listar(FILTRO filtro);

    protected abstract String getJarterReport();

    protected abstract String getUrlSistema();

}
