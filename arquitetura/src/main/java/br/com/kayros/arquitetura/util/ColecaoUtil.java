package br.com.kayros.arquitetura.util;

import org.apache.commons.collections4.IterableUtils;

import java.util.Objects;

public class ColecaoUtil {

    public static boolean isNotEmptyNull(Iterable lista) {

        return Objects.nonNull(lista) && IterableUtils.size(lista) > 0;
    }
}
