package br.com.kayros.arquitetura.relatorio.excel;

import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import br.com.kayros.arquitetura.relatorio.Relatorio;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifierContext;
import br.com.kayros.arquitetura.util.DateUtil;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Getter(AccessLevel.PROTECTED)
public abstract class RelatorioExcel<FILTRO> implements Relatorio<XSSFWorkbook, FILTRO> {

    private XSSFWorkbook relatorio;

    private RelatorioExcelEstilos estilos;

    @Override
    public XSSFWorkbook gerarRelatorio(FILTRO filtro) {
        criarWorkbook();

        XSSFSheet planilha = criarPlanilha(this.getTitulo());

        ajustarPaginacao(filtro);
        addTitulo(planilha);
        addConteudoRelatorio(planilha, filtro);
        addRodape(planilha);
        ajustarAutoSizeColumns(planilha);

        return relatorio;
    }

    private void ajustarAutoSizeColumns(XSSFSheet planilha) {
        for (int index = getPrimeiraColuna(); index <= getUltimaColuna(); index++) {
            planilha.autoSizeColumn(index);
        }
    }

    public byte[] getBytesRelatorio(FILTRO filtro) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            gerarRelatorio(filtro).write(byteArrayOutputStream);
        } catch (Exception e) {
            e.printStackTrace();

        }

        return byteArrayOutputStream.toByteArray();
    }

    private void criarWorkbook() {
        relatorio = new XSSFWorkbook();
        estilos = new RelatorioExcelEstilos(relatorio);
    }

    private XSSFSheet criarPlanilha(String titulo) {
        return relatorio.createSheet(titulo);
    }

    private void addTitulo(XSSFSheet planilha) {
        int numeroLinha = getProximaLinhaDisponivel(planilha);

        XSSFCell celulaTitulo = addCelula(planilha, numeroLinha, numeroLinha, getPrimeiraColuna(), getUltimaColuna(), estilos.getEstiloTitulo());
        celulaTitulo.setCellValue(this.getTitulo());

        getLinha(planilha, numeroLinha).setHeightInPoints(35);
    }

    private void addRodape(XSSFSheet planilha) {
        addLinhaEmBranco(planilha);

        String textoRedape = MensagemI18N.getKey("relatorio.geradoPorEm", new Object[]{AuthIdentifierContext.getCurrentAuthIdentifier().getIdentifierName(), DateUtil.formataDataHora(new Date())});

        int numeroLinha = getProximaLinhaDisponivel(planilha);
        addCelula(planilha, numeroLinha, numeroLinha, getPrimeiraColuna(), getUltimaColuna(), estilos.getEstiloRodape()).setCellValue(textoRedape);
    }

    private void ajustarPaginacao(FILTRO filtro) {
        if (filtro instanceof Paginacao) {
            Paginacao paginacao = (Paginacao) filtro;
            paginacao.setListarTodos(true);
            paginacao.setPreencherAcoes(false);
        }
    }

    protected void setCellFormula(XSSFCell celula, String formula) {
        celula.setCellFormula(formula);
        getRelatorio().getCreationHelper().createFormulaEvaluator().evaluateAll();
    }

    protected void addLinhaEmBranco(XSSFSheet planilha) {
        int numeroLinha = getProximaLinhaDisponivel(planilha);
        addCelula(planilha, numeroLinha, numeroLinha, getPrimeiraColuna(), getUltimaColuna());
    }

    protected void addLinhaNenhumRegistroEncontrado(XSSFSheet planilha) {
        int numeroLinha = getProximaLinhaDisponivel(planilha);
        addCelula(planilha, numeroLinha, numeroLinha, getPrimeiraColuna(), getUltimaColuna()).setCellValue(MensagemI18N.getKey("geral.nenhumRegistroEncontrado"));
    }

    protected void addLinhaTotalizacao(XSSFSheet planilha, Collection<?> objetos) {
        int numeroLinha = getProximaLinhaDisponivel(planilha);
        addCelula(planilha, numeroLinha, numeroLinha, getPrimeiraColuna(), getUltimaColuna(), estilos.getEstiloTextoDireitoNegrito())
                .setCellValue(MensagemI18N.getKey("geral.totalDeRegistros") + ": " + objetos.size());
    }

    protected XSSFCell addCelula(XSSFSheet planilha, int numeroLinha, int coluna) {
        return addCelula(planilha, numeroLinha, coluna, estilos.getEstiloTextoEsquerdo());
    }

    protected XSSFCell addCelula(XSSFSheet planilha, int numeroLinha, int coluna, XSSFCellStyle estilo) {
        XSSFCell celula = getCelula(getLinha(planilha, numeroLinha), coluna);
        celula.setCellStyle(estilo);
        return celula;
    }

    protected XSSFCell addCelula(XSSFSheet planilha, int numeroLinhaDe, int numeroLinhaAte, int colunaDe, int colunaAte) {
        return addCelula(planilha, numeroLinhaDe, numeroLinhaAte, colunaDe, colunaAte, estilos.getEstiloTextoEsquerdo());
    }

    protected XSSFCell addCelula(XSSFSheet planilha, int numeroLinhaDe, int numeroLinhaAte, int colunaDe, int colunaAte, XSSFCellStyle estilo) {
        planilha.addMergedRegion(new CellRangeAddress(numeroLinhaDe, numeroLinhaAte, colunaDe, colunaAte));

        for (int numeroLinhaTemp = numeroLinhaDe; numeroLinhaTemp <= numeroLinhaAte; numeroLinhaTemp++) {
            XSSFRow linha = getLinha(planilha, numeroLinhaTemp);

            for (int colunaTemp = colunaDe; colunaTemp <= colunaAte; colunaTemp++) {
                getCelula(linha, colunaTemp).setCellStyle(estilo);
            }
        }

        return getCelula(getLinha(planilha, numeroLinhaDe), colunaDe);
    }

    public void addCelulaMoeda(XSSFSheet planilha, Integer numeroLinha, Integer coluna, BigDecimal valor) {
        if (Objects.nonNull(valor)) {
            addCelula(planilha, numeroLinha, coluna, getEstilos().getEstiloTextoMoeda()).setCellValue(valor.doubleValue());
        } else {
            addCelula(planilha, numeroLinha, coluna, getEstilos().getEstiloTextoMoeda()).setCellValue("");
        }
    }

    protected int getProximaLinhaDisponivel(XSSFSheet planilha) {
        return Objects.isNull(planilha.getRow(planilha.getLastRowNum())) ? planilha.getLastRowNum() : planilha.getLastRowNum() + 1;
    }

    protected XSSFRow getLinha(XSSFSheet planilha, int numeroLinha) {
        XSSFRow linha = planilha.getRow(numeroLinha);

        if (Objects.isNull(linha)) {
            linha = planilha.createRow(numeroLinha);
        }

        return linha;
    }

    protected XSSFCell getCelula(XSSFRow linha, int coluna) {
        XSSFCell celula = linha.getCell(coluna);

        if (Objects.isNull(celula)) {
            celula = linha.createCell(coluna);
        }

        return celula;
    }

    protected abstract void addConteudoRelatorio(XSSFSheet planilha, FILTRO filtro);

    protected abstract String getTitulo();

    protected abstract int getPrimeiraColuna();

    protected abstract int getUltimaColuna();

}
