package br.com.kayros.arquitetura.rest.auth;

public class AuthIdentifierContext {

    private static ThreadLocal<AuthIdentifier> currentTenant = new ThreadLocal<>();

    @SuppressWarnings("unchecked")
    public static <A extends AuthIdentifier> A getCurrentAuthIdentifier() {
        return (A) currentTenant.get();
    }

    public static void setCurrentAuthIdentifier(AuthIdentifier tenant) {

        currentTenant.set(tenant);
    }
}
