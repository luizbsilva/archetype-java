package br.com.kayros.arquitetura.persistencia.impl;

import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.modelo.Entidade;
import br.com.kayros.arquitetura.modelo.EntidadeAuditavel;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.persistencia.DAO;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifierContext;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.arquitetura.util.Util;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.Session;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Transactional
public abstract class DAOImpl<E extends ObjectID> extends DAOConsultarImpl<E> implements DAO<E> {

    protected final Session getSession() {
        return getEntityManager().unwrap(Session.class);
    }

    protected void beforeSalvarAlterar(E entidade) throws RegraNegocioException {

    }

    public void alterarAtributos(E entidade, String... atributos) {
        StringBuilder sqlUpdate = new StringBuilder();

        Table table = entidade.getClass().getAnnotation(Table.class);

        sqlUpdate.append("update ");
        if (!StringUtils.isEmpty(table.schema())) {
            sqlUpdate.append(table.schema()).append(".");
        }
        sqlUpdate.append(table.name()).append(" set ");

        Map<String, Object> valoresPorParametro = new HashMap<>();

        Arrays.stream(atributos).forEach(atributo -> {
            Field field = FieldUtils.getField(entidade.getClass(), atributo, true);

            String nomeColune;
            nomeColune = preencherNomeColuna(field);

            Object valor = Util.getValorFromField(entidade, field);


            sqlUpdate.append(nomeColune).append(" = ").append(Objects.isNull(valor) ? "null" : ":" + atributo).append(", ");

            if (!Objects.isNull(valor)) {
                valor = preencharValorAtributo(field, valor);

                valoresPorParametro.put(atributo, valor);
            }
        });

        sqlUpdate.delete(sqlUpdate.length() - 2, sqlUpdate.length());
        sqlUpdate.append(" where ").append(getNameIdProperty()).append(" = :id");
        valoresPorParametro.put("id", entidade.getId());

        Query query = getEntityManager().createNativeQuery(sqlUpdate.toString());
        valoresPorParametro.forEach(query::setParameter);
        query.executeUpdate();
    }

    private Object preencharValorAtributo(Field field, Object valor) {
        if (valor.getClass().isEnum()) {
            Enumerated enumerated = field.getAnnotation(Enumerated.class);

            Enum valorEnum = (Enum) valor;
            valor = enumerated.value() == EnumType.ORDINAL ? valorEnum.ordinal() : valorEnum.name();

        } else if (valor instanceof ObjectID) {
            valor = ((ObjectID) valor).getId();

        }
        return valor;
    }

    private String preencherNomeColuna(Field field) {
        return preencherJoinColuna(field);
    }

    private String preencherJoinColuna(Field field) {
        String nomeColune;
        if (field.isAnnotationPresent(JoinColumn.class)) {
            JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
            nomeColune = joinColumn.name();

        } else if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            nomeColune = column.name().length() > 0 ? column.name() : field.getName();

        } else {
            nomeColune = field.getName();

        }
        return nomeColune;
    }

    @Override
    public void salvar(E entidade) throws RegraNegocioException {
        beforeSalvarAlterar(entidade);
        setarCriadoPor(entidade);
        ((Entidade) entidade).ativar();

        getEntityManager().persist(entidade);
        getEntityManager().flush();

        afterSalvarAlterar(entidade);
    }


    private void setarCriadoPor(E entidade) {
        if (entidade instanceof EntidadeAuditavel) {
            AuthIdentifier authIdentifier = AuthIdentifierContext.getCurrentAuthIdentifier();
            ((EntidadeAuditavel) entidade).setCriadoPor(Objects.isNull(authIdentifier) ? "Sistema" : authIdentifier.getIdentifierName());
        }
    }

    @Override
    public void alterar(E entidade) throws RegraNegocioException {
        beforeSalvarAlterar(entidade);
        setarAlteradoPor(entidade);

        getEntityManager().merge(entidade);
        getEntityManager().flush();

        afterSalvarAlterar(entidade);
    }

    private void setarAlteradoPor(E entidade) {
        if (entidade instanceof EntidadeAuditavel) {
            AuthIdentifier authIdentifier = AuthIdentifierContext.getCurrentAuthIdentifier();
            ((EntidadeAuditavel) entidade).setAtualizadoPor(Objects.isNull(authIdentifier) ? "Sistema" : authIdentifier.getIdentifierName());
        }
    }

    @Override
    public void excluir(Long id) throws RegraNegocioException {
        try {
            E entidade = this.entidadeClass().newInstance();
            entidade.setId(id);
            ((Entidade) entidade).desativar();
            alterarAtributos(entidade, "status");

        } catch (Exception e) {
            throw RegraNegocioException.build(MensagemI18N.getKey("operacao.excluir.falha"));
        }
    }

    public void excluirDefinitivamente(Long id) {
        E entidade = getSession().load(entidadeClass(), id);
        getSession().delete(entidade);
        getSession().flush();
    }

    protected void afterSalvarAlterar(E entidade) {
    }
}
