package br.com.kayros.arquitetura.modelo;

import java.util.Date;

public interface Log extends Entidade {

    Date getData();

    void setData(Date data);

    String getUsuario();

    void setUsuario(String usuario);

    String getObservacao();

    void setObservacao(String observacao);

}
