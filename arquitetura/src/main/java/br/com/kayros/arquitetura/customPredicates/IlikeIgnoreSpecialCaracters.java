package br.com.kayros.arquitetura.customPredicates;

import org.hibernate.criterion.MatchMode;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.ParameterRegistry;
import org.hibernate.query.criteria.internal.Renderable;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;
import org.hibernate.query.criteria.internal.predicate.AbstractSimplePredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import java.io.Serializable;

public class IlikeIgnoreSpecialCaracters extends AbstractSimplePredicate implements Serializable {

    private final Expression<String> matchExpression;
    private final Expression<String> pattern;

    /**
     * Será considerado {@link MatchMode#EXACT}
     *
     * @param criteriaBuilder um CriteriaBuilder
     * @param matchExpression um Expression
     * @param pattern         um String
     * @return um AbstractSimplePredicate
     */
    public static IlikeIgnoreSpecialCaracters build(CriteriaBuilder criteriaBuilder, Expression<String> matchExpression, String pattern) {
        return build(criteriaBuilder, matchExpression, pattern, MatchMode.EXACT);
    }

    public static IlikeIgnoreSpecialCaracters build(CriteriaBuilder criteriaBuilder, Expression<String> matchExpression, String pattern, MatchMode matchMode) {
        return new IlikeIgnoreSpecialCaracters((CriteriaBuilderImpl) criteriaBuilder, matchExpression, pattern, matchMode);
    }

    private IlikeIgnoreSpecialCaracters(CriteriaBuilderImpl criteriaBuilder, Expression<String> matchExpression, String pattern, MatchMode matchMode) {
        super(criteriaBuilder);

        this.matchExpression = matchExpression;
        this.pattern = new LiteralExpression<>(criteriaBuilder, matchMode.toMatchString(pattern));
    }

    @Override
    public void registerParameters(ParameterRegistry registry) {
        Helper.possibleParameter(null, registry);
        Helper.possibleParameter(this.matchExpression, registry);
        Helper.possibleParameter(this.pattern, registry);
    }

    @Override
    public String render(boolean isNegated, RenderingContext renderingContext) {
        final String operator = isNegated ? " not like " : " like ";
        StringBuilder sql = new StringBuilder();
        sql.append("lower(");
        sql.append("sem_acentos(");
        sql.append(((Renderable) this.matchExpression).render(renderingContext));
        sql.append(")");
        sql.append(")");
        sql.append(operator);
        sql.append(((Renderable) this.pattern).render(renderingContext));

        return sql.toString();
    }

}
