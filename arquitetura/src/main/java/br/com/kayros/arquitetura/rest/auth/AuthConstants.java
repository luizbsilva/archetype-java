package br.com.kayros.arquitetura.rest.auth;

public class AuthConstants {

    public static final String TOKEN_USER = "TOKEN.USER";

    public static final String TOKEN_USER_AUTHORIZATION = "TOKEN.USER.AUTHORIZATION";

}
