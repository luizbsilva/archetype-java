package br.com.kayros.arquitetura.service;

import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.util.RegraNegocioException;

import java.io.Serializable;

public interface Service<E extends ObjectID> extends ServiceConsultar<E> {

    void salvar(E entidade) throws RegraNegocioException;

    void alterar(E entidade) throws RegraNegocioException;

    void excluir(Long id) throws RegraNegocioException;

    void excluirDefinitivamente(Long id);

}
