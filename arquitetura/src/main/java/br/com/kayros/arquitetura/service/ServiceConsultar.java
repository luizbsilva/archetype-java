package br.com.kayros.arquitetura.service;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.dto.PaginacaoResultado;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;
import java.util.List;

public interface ServiceConsultar<E extends ObjectID> {

    E get(Long id);

    E get(String atributo, Object object);

    E get(Specification<E> specification);

    List<E> listar();

    PaginacaoResultado<E> listar(Paginacao paginacao);

    <T> List<T> dtoListar();

    PaginacaoResultado<?> dtoListar(Paginacao paginacao);

    List<MenuAcao> getAcoes(E entidade, Pagina pagina);

    List<MenuAcao> getAcoesTabelaListagem(E entidade);

    <T> T getValorAtributo(String atributo, Serializable id);

    <T> T getValorAtributo(String atributo, Specification<E> specification);
}
