package br.com.kayros.arquitetura.anotation;

import javax.persistence.criteria.JoinType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface Join {

    String associacao();

    String apelido();

    JoinType tipoJuncao() default JoinType.INNER;
}