package br.com.kayros.arquitetura.modelo;

import br.com.kayros.arquitetura.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Entidade extends ObjectID<Long> {

    Status getStatus();

    void setStatus(Status status);

    default void ativar() {
        this.setStatus(Status.ATIVO);
    }

    default void desativar() {
        this.setStatus(Status.INATIVO);
    }

    @JsonIgnore
    default boolean isAtivo() {
        return Status.ATIVO.equals(this.getStatus());
    }
}
