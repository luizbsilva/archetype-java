package br.com.kayros.arquitetura.rest.auth;

import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public interface AuthIdentifier {

    Long getIdentifier();

    String getIdentifierName();

    String getToken();

    void setToken(String token);

    Boolean getAdmin();

    Map<Modulo, Set<Funcionalidade>> getFuncionalidadesPorModulo();

    static AuthIdentifier build(String nome) {
        return new AuthIdentifier() {
            @Override
            public Long getIdentifier() {
                return new Random().nextLong();
            }

            @Override
            public String getIdentifierName() {
                return nome;
            }

            @Override
            public String getToken() {
                return null;
            }

            @Override
            public void setToken(String token) {
            }

            @Override
            public Boolean getAdmin() {
                return false;
            }

            @Override
            public Map<Modulo, Set<Funcionalidade>> getFuncionalidadesPorModulo() {
                return new HashMap<>();
            }
        };
    }

}