package br.com.kayros.arquitetura.service.impl;

import br.com.kayros.arquitetura.rest.auth.AuthConstants;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

@Service
public class AuthService {

    @Value("${kayros.security.jwt.key}")
    private String jwtKey;

    @Value("${kayros.security.jwt.issuer}")
    private String issuer;

    public String generateToken(AuthIdentifier authIdentifier) throws JsonProcessingException {
        Date dataAtual = Date.from(Instant.now());

        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder.setSubject(authIdentifier.getIdentifier().toString());
        jwtBuilder.setIssuedAt(dataAtual);
        jwtBuilder.setIssuer(issuer);
        jwtBuilder.claim(AuthConstants.TOKEN_USER, new ObjectMapper().writeValueAsString(authIdentifier));
        jwtBuilder.signWith(SignatureAlgorithm.HS512, jwtKey);

        return jwtBuilder.compact();
    }

    public boolean validarToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtKey).parse(token);
            return true;

        } catch (Exception e) {

            return false;
        }
    }

    public <T extends AuthIdentifier> T getAuthIdentifierFromToken(String token, Class<T> classAuthIdentifier) throws IOException {
        Claims claims = Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).getBody();

        return new ObjectMapper().readerFor(classAuthIdentifier).readValue((String) claims.get(AuthConstants.TOKEN_USER));
    }
}
