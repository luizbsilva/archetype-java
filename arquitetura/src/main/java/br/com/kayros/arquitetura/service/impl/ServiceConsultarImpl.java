package br.com.kayros.arquitetura.service.impl;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.dto.PaginacaoResultado;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import br.com.kayros.arquitetura.persistencia.DAOConsultar;
import br.com.kayros.arquitetura.service.ServiceConsultar;
import br.com.kayros.arquitetura.util.Util;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;


public abstract class ServiceConsultarImpl<E extends ObjectID> implements ServiceConsultar<E> {

    @Override
    public E get(Long id) {
        return getDAO().get(id);
    }

    @Override
    public E get(String atributo, Object object) {
        return getDAO().get(atributo, object);
    }

    @Override
    public E get(Specification<E> specification) {
        return getDAO().get(specification);
    }

    @Override
    public List<E> listar() {
        return getDAO().listar();
    }

    @Override
    public PaginacaoResultado<E> listar(Paginacao paginacao) {
        PaginacaoResultado<E> paginacaoResultado = getDAO().listar(paginacao);

        if (paginacao.getPreencherAcoes()) {
            paginacaoResultado.getEntidades().forEach(entidade -> paginacaoResultado.addAcoesTabelaListagem(entidade.getId().toString(), getAcoesTabelaListagem(entidade)));
        }

        return paginacaoResultado;
    }

    @Override
    public <T> List<T> dtoListar() {
        return getDAO().dtoListar();
    }

    @Override
    public PaginacaoResultado<?> dtoListar(Paginacao paginacao) {
        PaginacaoResultado<?> paginacaoResultado = getDAO().dtoListar(paginacao);

        if (paginacao.getPreencherAcoes()) {
            paginacaoResultado.getEntidades().forEach(dto -> {
                try {
                    E entidade = instanciarEntidade();
                    Util.copiarAtributos(dto, entidade);
                    paginacaoResultado.addAcoesTabelaListagem(entidade.getId().toString(), getAcoesTabelaListagem(entidade));
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            });
        }

        return paginacaoResultado;
    }

    @Override
    public List<MenuAcao> getAcoes(E entidade, Pagina pagina) {
        return Collections.emptyList();
    }

    @Override
    public List<MenuAcao> getAcoesTabelaListagem(E entidade) {
        return Collections.emptyList();
    }

    @Override
    public <T> T getValorAtributo(String atributo, Serializable id) {
        return getDAO().getValorAtributo(atributo, id);
    }

    @Override
    public <T> T getValorAtributo(String atributo, Specification<E> specification) {
        return getDAO().getValorAtributo(atributo, specification);
    }

    public E fromDtoListar(Object dto) {
        return null;
    }

    @SuppressWarnings("unchecked")
    private E instanciarEntidade() {
        E entidade = null;
        try {
            Class<E> classe = (Class<E>) Util.getTypes(this)[0];
            entidade = classe.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return entidade;
    }

    protected abstract DAOConsultar<E> getDAO();

    protected <T> ResponseEntity<T> getResponseEntityFrom(String url, HttpMethod httpMethod, HttpEntity<?> requestUpdate, Class<T> retornType) {
        return new RestTemplate().exchange(url, httpMethod, requestUpdate, retornType);
    }

}
