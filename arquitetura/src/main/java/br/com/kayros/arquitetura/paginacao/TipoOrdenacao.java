package br.com.kayros.arquitetura.paginacao;

public enum TipoOrdenacao {
    ASC,
    DESC
}
