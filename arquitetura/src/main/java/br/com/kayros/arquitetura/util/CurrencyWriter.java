package br.com.kayros.arquitetura.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CurrencyWriter {
    private static final String CENTO = "cento";
    private static final String CEM = "cem";

    private final Map<Integer, String> grandezasPlural = new HashMap<>();
    private final Map<Integer, String> grandezasSingular = new HashMap<>();
    private final Map<Integer, String> nomes = new HashMap<>();

    private static final String MOEDA_SINGULAR = "real";
    private static final String MOEDA_PLURAL = "reais";

    private static final String FRACAO_SINGULAR = "centavo";
    private static final String FRACAO_PLURAL = "centavos";

    private static final String PARTICULA_ADITIVA = " e ";

    private static final BigDecimal MAX_SUPPORTED_VALUE = new BigDecimal("999999999999999999999999999.99");

    private static CurrencyWriter instance = null;

    public static String write(BigDecimal valor) {
        String valorExtenso = "";

        if (!Objects.isNull(valor)) {
            valorExtenso = getInstance().transformer(valor);
        }

        return valorExtenso;
    }

    private static CurrencyWriter getInstance() {
        if (instance == null) {
            instance = new CurrencyWriter();
        }

        return instance;
    }

    private CurrencyWriter() {
        preencherGrandezasPlural();
        preencherGrandezasSingular();
        preencherNomes();
    }

    private String transformer(final BigDecimal valor) {
        StringBuilder valorPorExtenso = new StringBuilder();

        if (null == valor) {
            throw new IllegalArgumentException();

        } else if (MAX_SUPPORTED_VALUE.compareTo(valor) < 0) {
            throw new IllegalArgumentException("Valor acima do limite suportado.");

        } else if (BigDecimal.ZERO.compareTo(valor) == 0) {
            valorPorExtenso.append("zero " + MOEDA_SINGULAR);

        } else if (BigDecimal.ZERO.compareTo(valor) < 0) {
            NumberFormat numberFormat = NumberFormat.getInstance();
            numberFormat.setMinimumFractionDigits(2);

            String valorTexto = numberFormat.format(valor.doubleValue()).replace(",", ".");

            String[] partes = valorTexto.split("\\.");
            Integer qtdeParter = partes.length;

            for (int index = 0; index < partes.length; index++) {
                Integer parte = Integer.valueOf(partes[index]);

                if (parte > 0) {
                    if (index > 0) {
                        valorPorExtenso.append(index + 1 == qtdeParter ? PARTICULA_ADITIVA : " ");
                    }

                    String nomeGrupo = comporNomeGrupos(parte).toString();

                    if (terminaComMil(valorPorExtenso) && (parte <= 100 && !nomeGrupo.equals(CENTO))) {
                        valorPorExtenso.append(PARTICULA_ADITIVA);
                    }

                    valorPorExtenso.append(nomeGrupo);
                    valorPorExtenso.append(" ");
                    valorPorExtenso.append(obterNomeGrandeza(qtdeParter - index, parte));
                }

                if (index == qtdeParter - 1 && parte > 0) {
                    valorPorExtenso.append(parte > 1 ? FRACAO_PLURAL : FRACAO_SINGULAR);

                } else if (index == qtdeParter - 2) {
                    boolean terminaComEspaco = valorPorExtenso.lastIndexOf(" ") == valorPorExtenso.length() - 1;
                    valorPorExtenso.append(terminaComEspaco ? "" : " ").append(MOEDA_PLURAL);

                }
            }
        }

        Util.replaceAll(valorPorExtenso, "um mil ", "mil ");
        Util.replaceAll(valorPorExtenso, "  ", " ");

        return valorPorExtenso.toString();
    }

    private boolean terminaComMil(StringBuilder valorPorExtenso) {

        boolean terminalComMil;
        try {
            terminalComMil = valorPorExtenso.substring(valorPorExtenso.length() - 4, valorPorExtenso.length()).equals("mil ");

        } catch (StringIndexOutOfBoundsException ignored) {
            terminalComMil = false;

        }

        return terminalComMil;
    }


    private StringBuilder comporNomeGrupos(int valor) {
        StringBuilder nome = new StringBuilder();

        int centenas = valor - (valor % 100);
        int unidades = valor % 10;
        int dezenas = (valor - centenas) - unidades;
        int duasCasas = dezenas + unidades;

        if (centenas > 0) {
            nome.append(PARTICULA_ADITIVA);

            if (100 == centenas) {
                if (duasCasas > 0) {
                    nome.append(CENTO);
                } else {
                    nome.append(CEM);
                }
            } else {
                nome.append(nomes.get(centenas));
            }
        }

        if (duasCasas > 0) {
            nome.append(PARTICULA_ADITIVA);
            if (duasCasas < 20) {
                nome.append(nomes.get(duasCasas));
            } else {
                if (dezenas > 0) {
                    nome.append(nomes.get(dezenas));
                }

                if (unidades > 0) {
                    nome.append(PARTICULA_ADITIVA);
                    nome.append(nomes.get(unidades));
                }
            }
        }

        return nome.delete(0, 3);
    }

    private String obterNomeGrandeza(int exponent, int value) {
        if (exponent < 3) {
            return "";
        } else if (value == 1) {
            return grandezasSingular.get(exponent);
        } else {
            return grandezasPlural.get(exponent);
        }
    }

    private void preencherGrandezasPlural() {
        grandezasPlural.put(3, "mil");
        grandezasPlural.put(4, "milhões");
        grandezasPlural.put(5, "bilhões");
        grandezasPlural.put(6, "trilhões");
        grandezasPlural.put(7, "quatrilhões");
        grandezasPlural.put(8, "quintilhões");
        grandezasPlural.put(9, "sextilhões");
        grandezasPlural.put(10, "setilhões");
    }

    private void preencherGrandezasSingular() {
        grandezasSingular.put(3, "mil");
        grandezasSingular.put(4, "milhão");
        grandezasSingular.put(5, "bilhão");
        grandezasSingular.put(6, "trilhão");
        grandezasSingular.put(7, "quatrilhão");
        grandezasSingular.put(8, "quintilhão");
        grandezasSingular.put(9, "sextilhão");
        grandezasSingular.put(10, "setilhão");
    }

    private void preencherNomes() {
        nomes.put(1, "um");
        nomes.put(2, "dois");
        nomes.put(3, "três");
        nomes.put(4, "quatro");
        nomes.put(5, "cinco");
        nomes.put(6, "seis");
        nomes.put(7, "sete");
        nomes.put(8, "oito");
        nomes.put(9, "nove");
        nomes.put(10, "dez");
        nomes.put(11, "onze");
        nomes.put(12, "doze");
        nomes.put(13, "treze");
        nomes.put(14, "quatorze");
        nomes.put(15, "quinze");
        nomes.put(16, "dezesseis");
        nomes.put(17, "dezessete");
        nomes.put(18, "dezoito");
        nomes.put(19, "dezenove");
        nomes.put(20, "vinte");
        nomes.put(30, "trinta");
        nomes.put(40, "quarenta");
        nomes.put(50, "cinquenta");
        nomes.put(60, "sessenta");
        nomes.put(70, "setenta");
        nomes.put(80, "oitenta");
        nomes.put(90, "noventa");
        nomes.put(200, "duzentos");
        nomes.put(300, "trezentos");
        nomes.put(400, "quatrocentos");
        nomes.put(500, "quinhentos");
        nomes.put(600, "seiscentos");
        nomes.put(700, "setecentos");
        nomes.put(800, "oitocentos");
        nomes.put(900, "novecentos");
    }
}
