package br.com.kayros.arquitetura.dto.impl;

import br.com.kayros.arquitetura.anotation.ProjectionProperty;
import br.com.kayros.arquitetura.dto.DTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class EntidadeDTO<E> implements DTO<Long> {

    @ProjectionProperty("id")
    private Long id;

}
