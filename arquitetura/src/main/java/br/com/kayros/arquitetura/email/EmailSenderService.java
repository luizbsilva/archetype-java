package br.com.kayros.arquitetura.email;

import br.com.kayros.arquitetura.util.Constantes;
import br.com.kayros.arquitetura.util.ThreadPool;
import br.com.kayros.arquitetura.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
public abstract class EmailSenderService {

    public static final String PARAMETRO_URL_SISTEMA = "${url_sistema}";

    private static Queue<Email> historicoEmails = new LinkedList<>();

    @Value("${spring.profiles.active}")
    private String profile;

    @Value("${kayros.ambiente.dev}")
    private Boolean ambienteDev;

    public void sendEmail(Email email) {
        sendEmail(email, getConfiguracao());
    }

    public void sendEmail(Email email, EmailSenderConfiguration configuracao) {
        if (isProfileTeste()) {
            send(email, configuracao);

        } else {
            ThreadPool.enfileirarTarefaFixed(() -> send(email, configuracao));
        }
    }

    public void sendEmailTeste(Email email, EmailSenderConfiguration configuracao) {
        send(email, configuracao);
    }

    private void send(Email email, EmailSenderConfiguration configuracao) {

        if (configuracao.possuiTodasInformacoess()) {
            List<String> logs = new ArrayList<>();

            try {
                Session mailSession = createMailSession(configuracao);

                String assunto = substituirParametros(email, email.getAssunto(), false);
                String textoEmailHtml = getTextoEmailPadronizado(email);
                String destinatarios = getDestinatarios(email.getDestinatario());
                String destinatariosCC = getDestinatarios(email.getCC());
                String destinatariosBCC = getDestinatarios(email.getBCC());

                MimeMessage message = new MimeMessage(mailSession);
                message.setFrom(new InternetAddress(configuracao.getRemetente()));
                message.addRecipients(Message.RecipientType.CC, destinatariosCC);
                message.addRecipients(Message.RecipientType.BCC, destinatariosBCC);
                message.addRecipients(Message.RecipientType.TO, destinatarios);
                message.setSubject(assunto, "UTF-8");

                Multipart corpoEmail = new MimeMultipart();
                addTextoEmail(corpoEmail, textoEmailHtml);
                addAnexos(corpoEmail, email.getAnexos());
                message.setContent(corpoEmail);

                logs.add("---- Enviando e-mail ----");
                logs.add("Id: " + email.getId());
                logs.add("From: " + configuracao.getRemetente());
                logs.add("To: " + destinatarios);

                if (!StringUtils.isEmpty(destinatariosCC)) {
                    logs.add("CC: " + destinatariosCC);
                }

                if (!StringUtils.isEmpty(destinatariosBCC)) {
                    logs.add("BCC: " + destinatariosBCC);
                }

                logs.add("Subject: " + assunto);
                logs.add("Text: " + textoEmailHtml);
                logs.add("Anexos: " + getNomesAnexos(email));

                logs.add("Profile: " + profile);
                if (!ambienteDev) {
                    Transport.send(message);
                    apagarArquivosAnexos(email);
                } else {
                    EmailSenderService.salvarHistoricoEmail(email);
                }

                logs.add("Status: Email Enviado");
                logs.add("Ambiente de Desenvolvimento: " + ambienteDev + " (se igual a 'true' os e-mails não são enviados de verdade)");
            } catch (Exception e) {
                logs.add("Status: Email Não Enviado:");

                log.error("Erro ao Enviar Email:", e);
                throw new RuntimeException(e);

            }
            logs.add("-------------------------");
            pringLogs(logs);

        } else {
            log.warn("---- O sistema não possui todas as configurações de e-mail ----");

        }

    }

    private boolean isProfileTeste() {
        return Constantes.SPRING_PROFILE_TESTE.equals(profile);
    }

    private synchronized void pringLogs(List<String> logs) {
        logs.forEach(log::info);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void apagarArquivosAnexos(Email email) {
        if (!email.getAnexos().isEmpty()) {
            email.getAnexos().forEach(file -> {
                if (file.exists()) {
                    file.delete();
                }
            });
        }
    }

    private String getNomesAnexos(Email email) {
        String nomesAnexos = "-";

        if (!email.getAnexos().isEmpty()) {
            nomesAnexos = email.getAnexos().stream().map(File::getName).reduce("", (nomeConcatenado, nome) -> {
                if (nomeConcatenado.equals("")) {
                    return nome;
                } else {
                    return nomeConcatenado + ", " + nome;
                }
            });
        }

        return nomesAnexos;
    }

    private void addTextoEmail(Multipart corpoEmail, String textoEmailHtml) throws Exception {
        MimeBodyPart textoEmail = new MimeBodyPart();
        textoEmail.setContent(textoEmailHtml, "text/html; charset=UTF-8");

        corpoEmail.addBodyPart(textoEmail);
    }

    private void addAnexos(Multipart corpoEmail, List<File> anexos) throws Exception {
        AtomicBoolean falhaAoAnexarArquivos = new AtomicBoolean(false);

        anexos.forEach(file -> {
            try {
                MimeBodyPart anexo = new MimeBodyPart();
                anexo.attachFile(file);

                corpoEmail.addBodyPart(anexo);

            } catch (Exception e) {
                falhaAoAnexarArquivos.set(true);
                log.error("Falha ao anexos o arquivo:" + file.getName(), e);

            }
        });

        if (falhaAoAnexarArquivos.get()) {
            throw new Exception("Não foi possível anexos todos os arquivos no email.");
        }
    }

    private String getDestinatarios(List<String> destinatarios) {
        return destinatarios.stream().collect(Collectors.joining(", "));
    }

    private String getTextoEmailPadronizado(Email email) {
        return "<font color='#000000' size='2' face='Verdana'>" + substituirParametros(email, email.getTextoEmail()) + getRodape() + "</font>";
    }

    private String substituirParametros(Email email, String valor) {
        return substituirParametros(email, valor, true);
    }

    private String substituirParametros(Email email, String valor, boolean escapeHtml) {
        StringBuilder novoValor = new StringBuilder(valor);

        Map<String, String> parametros = email.getParametros();
        parametros.put(PARAMETRO_URL_SISTEMA, getConfiguracao().getUrlSistema());

        parametros.forEach((key, value) -> Util.replaceAll(novoValor, key, escapeHtml ? StringEscapeUtils.escapeHtml4(value) : value));

        return novoValor.toString();
    }

    private String getRodape() {
        return EmailBodyBuilder
                .build()
                .addLineBreak(2)
                .addLineDivider()
                .open_P_element("text-align: right")
                .addDataHora(new Date())
                .close_P_element()
                .toString();
    }

    private Session createMailSession(EmailSenderConfiguration configuracao) {
        Session mailSession;

        Properties properties = new Properties();
        properties.put("mail.smtp.host", configuracao.getHost());
        properties.put("mail.smtp.port", configuracao.getPorta());
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.debug", "true");

        if (!StringUtils.isEmpty(configuracao.getPorta())) {
            properties.put("mail.smtp.port", configuracao.getPorta());
        }

        if (configuracao.isProtocoloSSL()) {
            properties.put("mail.smtp.ssl.enable", true);
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.starttls.enable", "true");

            if (!StringUtils.isEmpty(configuracao.getPorta())) {
                properties.put("mail.smtp.ssl.socketFactory.port", configuracao.getPorta());

            } else {
                properties.put("mail.smtp.port", "465");
                properties.put("mail.smtp.socketFactory.port", "465");

            }

        } else {
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.starttls.enable", "true");
        }

        mailSession = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(configuracao.getUsuario(), configuracao.getSenha());
            }
        });

        return mailSession;
    }

    public static void salvarHistoricoEmail(Email email) {
        if (EmailSenderService.historicoEmails.size() > 5) {
            EmailSenderService.historicoEmails.poll();
        }

        EmailSenderService.historicoEmails.add(email);
    }

    public static void limparHistoricoEmail() {
        EmailSenderService.historicoEmails.clear();
    }

    public static Queue<Email> getHistoricoEmails() {
        return historicoEmails;
    }

    public abstract EmailSenderConfiguration getConfiguracao();
}
