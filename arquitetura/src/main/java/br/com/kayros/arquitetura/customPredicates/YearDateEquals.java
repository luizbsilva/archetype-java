package br.com.kayros.arquitetura.customPredicates;

import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.ParameterRegistry;
import org.hibernate.query.criteria.internal.Renderable;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;
import org.hibernate.query.criteria.internal.predicate.AbstractSimplePredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import java.io.Serializable;
import java.util.Date;


public class YearDateEquals extends AbstractSimplePredicate implements Serializable {

    private final Expression<Date> matchExpression;
    private final Expression<Integer> pattern;

    public static YearDateEquals build(CriteriaBuilder criteriaBuilder, Expression<Date> matchExpression, Integer ano) {
        return new YearDateEquals((CriteriaBuilderImpl) criteriaBuilder, matchExpression, ano);
    }

    private YearDateEquals(CriteriaBuilderImpl criteriaBuilder, Expression<Date> matchExpression, Integer ano) {
        super(criteriaBuilder);

        this.matchExpression = matchExpression;
        this.pattern = new LiteralExpression<>(criteriaBuilder, ano);
    }

    @Override
    public void registerParameters(ParameterRegistry registry) {
        Helper.possibleParameter(null, registry);
        Helper.possibleParameter(this.matchExpression, registry);
        Helper.possibleParameter(this.pattern, registry);
    }

    @Override
    public String render(boolean isNegated, RenderingContext renderingContext) {
        final String operator = isNegated ? " <> " : " = ";
        StringBuilder sql = new StringBuilder();
        sql.append("extract (");
        sql.append("year from ").append(((Renderable) this.matchExpression).render(renderingContext));
        sql.append(")");
        sql.append(operator);
        sql.append(((Renderable) this.pattern).render(renderingContext));

        return sql.toString();
    }

}
