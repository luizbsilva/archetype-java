package br.com.kayros.arquitetura.persistencia.impl;

import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.persistencia.DAO;
import br.com.kayros.arquitetura.util.RegraNegocioException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Transactional
public abstract class DAOSimplificadoImpl<E extends ObjectID> extends DAOImpl<E> implements DAO<E> {

    @Override
    public void salvar(E entidade) throws RegraNegocioException {
        beforeSalvarAlterar(entidade);
        getEntityManager().persist(entidade);
        getEntityManager().flush();
    }

    @Override
    public void alterar(E entidade) throws RegraNegocioException {
        beforeSalvarAlterar(entidade);
        getEntityManager().merge(entidade);
        getEntityManager().flush();
    }

    @Override
    public void excluir(Long id) {
        excluirDefinitivamente(id);
    }

    @Override
    public void excluirDefinitivamente(Long id) {
        E entidade = getSession().load(entidadeClass(), id);
        getSession().delete(entidade);
        getSession().flush();
    }

    protected List<Predicate> getPredicatesDefault(Root<?> root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return new ArrayList<>();
    }
}
