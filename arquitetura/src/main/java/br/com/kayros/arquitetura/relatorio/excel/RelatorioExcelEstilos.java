package br.com.kayros.arquitetura.relatorio.excel;

import lombok.Getter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.Color;

@Getter
public class RelatorioExcelEstilos {

    private XSSFWorkbook workBook;

    private static final int DATA_FORMAT_MOEDA = 8;

    private static final XSSFColor COR_BRANCO = new XSSFColor(IndexedColors.WHITE, new DefaultIndexedColorMap());
    private static final XSSFColor COR_PRETO = new XSSFColor(IndexedColors.BLACK, new DefaultIndexedColorMap());
    private static final XSSFColor COR_RED = new XSSFColor(new Color(170, 0, 0));
    private static final XSSFColor COR_GRAY_DARK = new XSSFColor(new Color(102, 102, 102));

    private XSSFCellStyle estiloTitulo;
    private XSSFCellStyle estiloTituloColuna;
    private XSSFCellStyle estiloRodape;
    private XSSFCellStyle estiloTextoCentro;
    private XSSFCellStyle estiloTextoDireito;
    private XSSFCellStyle estiloTextoDireitoNegrito;
    private XSSFCellStyle estiloTextoEsquerdo;
    private XSSFCellStyle estiloTextoMoeda;


    public RelatorioExcelEstilos(XSSFWorkbook workBook) {
        this.workBook = workBook;

        criarEstiloTitulo();
        criarEstiloTituloColuna();
        criarEstiloRodape();
        criarEstiloTextoCentro();
        criarEstiloTextoDireito();
        criarEstiloTextoDireitoNegrito();
        criarEstiloTextoEsquerdo();
        criarEstiloTextoMoeda();
    }

    public XSSFCellStyle criarEstiloPadrao() {
        XSSFCellStyle estiloPadrao = workBook.createCellStyle();
        estiloPadrao.setWrapText(true);
        estiloPadrao.setVerticalAlignment(VerticalAlignment.TOP);

        return estiloPadrao;
    }

    private void criarEstiloTitulo() {
        Font font = workBook.createFont();
        font.setFontHeightInPoints((short) 20);
        font.setBold(true);
        font.setColor(COR_BRANCO.getIndex());

        estiloTitulo = criarEstiloPadrao();
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER);
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);
        estiloTitulo.setFont(font);
        estiloTitulo.setFillForegroundColor(COR_RED);
        estiloTitulo.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

    private void criarEstiloTituloColuna() {
        Font font = workBook.createFont();
        font.setFontHeightInPoints((short) 14);
        font.setBold(true);
        font.setColor(COR_BRANCO.getIndex());

        estiloTituloColuna = criarEstiloPadrao();
        estiloTituloColuna.setAlignment(HorizontalAlignment.CENTER);
        estiloTituloColuna.setVerticalAlignment(VerticalAlignment.CENTER);
        estiloTituloColuna.setFont(font);
        estiloTituloColuna.setFillForegroundColor(COR_GRAY_DARK);
        estiloTituloColuna.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    }

    private void criarEstiloRodape() {
        estiloRodape = criarEstiloPadrao();
        estiloRodape.setAlignment(HorizontalAlignment.RIGHT);
        estiloRodape.setBorderTop(BorderStyle.DOTTED);
        estiloRodape.setTopBorderColor(COR_PRETO);
    }

    private void criarEstiloTextoCentro() {
        estiloTextoCentro = criarEstiloPadrao();
        estiloTextoCentro.setAlignment(HorizontalAlignment.CENTER);
    }

    private void criarEstiloTextoDireito() {
        estiloTextoDireito = criarEstiloPadrao();
        estiloTextoDireito.setAlignment(HorizontalAlignment.RIGHT);
    }

    private void criarEstiloTextoDireitoNegrito() {
        Font font = workBook.createFont();
        font.setBold(true);

        estiloTextoDireitoNegrito = criarEstiloPadrao();
        estiloTextoDireitoNegrito.setAlignment(HorizontalAlignment.RIGHT);
        estiloTextoDireitoNegrito.setFont(font);
    }

    private void criarEstiloTextoEsquerdo() {
        estiloTextoEsquerdo = criarEstiloPadrao();
    }

    private void criarEstiloTextoMoeda() {
        estiloTextoMoeda = criarEstiloPadrao();
        estiloTextoMoeda.setDataFormat(DATA_FORMAT_MOEDA);
    }
}
