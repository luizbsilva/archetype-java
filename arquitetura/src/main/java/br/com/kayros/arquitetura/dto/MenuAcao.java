package br.com.kayros.arquitetura.dto;

import br.com.kayros.arquitetura.enums.Funcionalidade;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


@Getter
@EqualsAndHashCode(of = {"label"})
@Builder
public class MenuAcao {

    private String label;

    private String icon;

    private String styleClassBotao;

    private String funcao;

    private Boolean submit;

    private Boolean exibirComoBotao;

    private Boolean desabilitado;

    private Set<Funcionalidade> funcionalidades = new HashSet<>();

    public MenuAcao clearFuncionalidade() {
        this.funcionalidades = new HashSet<>();
        return this;
    }

    public boolean hasFuncionalidade() {
        return !funcionalidades.isEmpty();
    }

    public Set<Funcionalidade> getFuncionalidadesDependentes() {
        final Set<Funcionalidade> funcionalidadesDependentes = new HashSet<>();
        if (funcionalidades == null)
            funcionalidades = new HashSet<>();
        funcionalidades.forEach(funcionalidade -> funcionalidadesDependentes.addAll(funcionalidade.getFuncionalidadesDependentes()));

        return funcionalidadesDependentes;
    }

    public static Set toSet(Funcionalidade... funcionalidades) {
        return new HashSet(Arrays.asList(funcionalidades));
    }
}
