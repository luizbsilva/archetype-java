package br.com.kayros.arquitetura.modelo.impl;

import br.com.kayros.arquitetura.modelo.Arquivo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class ArquivoImpl extends EntidadeImpl implements Arquivo {

    @Column
    private String nome;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "base_64", columnDefinition = "TEXT")
    private String base64;

}
