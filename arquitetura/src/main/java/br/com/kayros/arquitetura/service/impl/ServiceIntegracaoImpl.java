package br.com.kayros.arquitetura.service.impl;

import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public abstract class ServiceIntegracaoImpl {

    protected void tratarResponseStatus(ResponseEntity respone, String mensagemQuandoErro) throws RegraNegocioException {
        if (respone.getStatusCode().isError()) {
            throw new RegraNegocioException(mensagemQuandoErro);
        }
    }

    protected Map<String, Object> getResponseBody(ResponseEntity<String> respone, String serviceAcessado) throws RegraNegocioException {
        Map<String, Object> responseBody;

        try {
            ObjectMapper mapper = new ObjectMapper();
            responseBody = mapper.readValue(respone.getBody(), new TypeReference<Map<String, Object>>() {
            });

        } catch (IOException e) {
            throw new RegraNegocioException(MensagemI18N.getKey("geral.erro.integracao", new Object[]{serviceAcessado}));

        }

        return responseBody;
    }

    protected List<Map<String, Object>> getResponseBodyList(ResponseEntity<String> respone, String serviceAcessado) throws RegraNegocioException {
        List<Map<String, Object>> responseBody;

        try {
            ObjectMapper mapper = new ObjectMapper();
            responseBody = mapper.readValue(respone.getBody(), new TypeReference<List<Map<String, Object>>>() {
            });

        } catch (IOException e) {
            throw new RegraNegocioException(MensagemI18N.getKey("geral.erro.integracao", new Object[]{serviceAcessado}));

        }

        return responseBody;
    }

    protected <T> ResponseEntity<T> getResponseEntityFrom(String url, HttpMethod httpMethod, HttpEntity<?> requestUpdate, Class<T> retornType) {
        return new RestTemplate().exchange(url, httpMethod, requestUpdate, retornType);
    }
}
