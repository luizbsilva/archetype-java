package br.com.kayros.arquitetura.modelo.impl;

import br.com.kayros.arquitetura.modelo.Log;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class LogImpl extends EntidadeImpl implements Log {

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column
    private Date data;

    @Column
    private String usuario;

    @Column(columnDefinition = "TEXT")
    private String observacao;

    public LogImpl(String usuario, String observacao) {
        this.usuario = usuario;
        this.observacao = observacao;
    }

}
