package br.com.kayros.arquitetura.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public interface ObjectID<ID extends Serializable> {

    ID getId();

    void setId(ID id);

    @JsonIgnore
    default boolean isNew() {
        return this.getId() == null;
    }
}
