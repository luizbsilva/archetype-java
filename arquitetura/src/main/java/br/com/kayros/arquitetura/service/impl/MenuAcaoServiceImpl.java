package br.com.kayros.arquitetura.service.impl;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.enums.Funcionalidade;
import br.com.kayros.arquitetura.enums.Modulo;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifier;
import br.com.kayros.arquitetura.rest.auth.AuthIdentifierContext;
import br.com.kayros.arquitetura.service.MenuAcaoService;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public abstract class MenuAcaoServiceImpl<E> implements MenuAcaoService<E> {

    private static final String LABEL_EDITAR = "Editar";
    private static final String LABEL_EXCLUIR = "Excluir";
    private static final String LABEL_ATIVAR = "Ativar";
    private static final String LABEL_INATIVAR = "Inativar";

    @Override
    public final List<MenuAcao> getAcoes(E entidade, Pagina pagina) {
        return filtrarAcoesPermitidasParaUsuarioLogado(getAcoesPorPagina(entidade, pagina));
    }

    @Override
    public final List<MenuAcao> getAcoesTabelaListagem(E entidade) {
        Set<MenuAcao> acoesFiltradas = new HashSet<>();

        // SE A AÇÃO ADICIONADA PARA A TELA DE LISTAGEM DEPENDE DA AÇÃO DE CONSULTAR. ADICIONA A AÇÃO DE CONSULTA NAS AÇÕES DA TABELA
        MenuAcao acaoConsultarTabelaListagem = getAcaoConsultarTabelaListagem();
        getAcoes(entidade, Pagina.LISTAGEM).forEach(acaoTemp -> {
            if (acaoTemp.getFuncionalidadesDependentes().contains(Funcionalidade.CONSULTAR) && !acoesFiltradas.contains(acaoConsultarTabelaListagem)) {
                acoesFiltradas.add(getAcaoConsultarTabelaListagem());
            }
        });

        filtrarAcoesPermitidasParaUsuarioLogado(getAcoesTabelaTelaListagem()).forEach(acoesFiltradas::add);

        return acoesFiltradas.stream().collect(Collectors.toList());
    }

    private List<MenuAcao> filtrarAcoesPermitidasParaUsuarioLogado(List<MenuAcao> acoes) {
        AuthIdentifier usuarioLogado = AuthIdentifierContext.getCurrentAuthIdentifier();

        List<MenuAcao> acoesFiltradas = new ArrayList<>();

        if (Boolean.FALSE.equals(usuarioLogado.getAdmin())) {
            Map<Modulo, Set<Funcionalidade>> funcionalidadesPorModuloUsuarioLogado = usuarioLogado.getFuncionalidadesPorModulo();

            acoes.forEach(acao -> {
                if (acao.getFuncionalidades() == null || (acao.getFuncionalidades() != null && acao.getFuncionalidades().isEmpty())) {
                    this.adicionarAcoesDependentes(acao, acoes, acoesFiltradas);

                    if (!acoesFiltradas.contains(acao)) {
                        acoesFiltradas.add(acao);
                    }

                } else if (funcionalidadesPorModuloUsuarioLogado.containsKey(getModulo())) {
                    filtarMenuAcaoPorModulo(acoes, acoesFiltradas, funcionalidadesPorModuloUsuarioLogado, acao);
                }
            });
        } else {
            acoesFiltradas.addAll(acoes);
        }

        return acoesFiltradas;
    }

    private void filtarMenuAcaoPorModulo(List<MenuAcao> acoes, List<MenuAcao> acoesFiltradas, Map<Modulo, Set<Funcionalidade>> funcionalidadesPorModuloUsuarioLogado, MenuAcao acao) {
        Collection<Funcionalidade> funcionalidadesUsuarioLogado = funcionalidadesPorModuloUsuarioLogado.get(getModulo());
        Collection<Funcionalidade> funcionalidadesAcao = acao.getFuncionalidades();
        funcionalidadesUsuarioLogado.forEach(funcionalidadeUsuarioLogado -> {
            if (funcionalidadesAcao.contains(funcionalidadeUsuarioLogado)) {
                this.adicionarAcoesDependentes(acao, acoes, acoesFiltradas);

                if (!acoesFiltradas.contains(acao)) {
                    acoesFiltradas.add(acao);
                }
            }
        });
    }

    private void adicionarAcoesDependentes(MenuAcao acao, List<MenuAcao> todasAcoes, List<MenuAcao> acoesFiltradas) {
        acao.getFuncionalidadesDependentes().forEach(funcionalidadeDependente ->
                todasAcoes.forEach(acaoDependente -> {
                    if (acaoDependente.getFuncionalidades() != null && acaoDependente.getFuncionalidades().contains(funcionalidadeDependente) && !acoesFiltradas.contains(acao)) {
                        acoesFiltradas.add(acaoDependente);
                    }
                })
        );
    }

    protected List<MenuAcao> getAcoesPorPagina(E entidade, Pagina pagina) {
        log.info("Entidade: " + entidade);
        List<MenuAcao> acoes = new ArrayList<>();

        switch (pagina) {
            case LISTAGEM:
                adicionarAcoesPaginaListagem(acoes);
                break;
            case CADASTRO:
                adicionarAcoesPaginaCadastro(acoes);
                break;
            default:
                adicionarAcoesPaginaConsultar(acoes);
                break;
        }

        return acoes;
    }


    protected List<MenuAcao> getAcoesTabelaTelaListagem() {
        List<MenuAcao> acoes = new ArrayList<>();

        acoes.add(getAcaoConsultarTabelaListagem());
        acoes.add(getAcaoEditarTabelaListagem());
        acoes.add(getAcaoExcluirTabelaListagem());

        return acoes;
    }

    protected void adicionarAcoesPaginaListagem(List<MenuAcao> acoes) {
        acoes.add(getAcaoNovo());
    }

    protected void adicionarAcoesPaginaCadastro(List<MenuAcao> acoes) {
        acoes.add(getAcaoSalvar());
        acoes.add(getAcaoVoltar());
    }

    public void adicionarAcoesPaginaConsultar(List<MenuAcao> acoes) {
        acoes.add(getAcaoVoltar());
    }

    public Boolean validarPermissao(Boolean isuserAdmin, Funcionalidade funcionalidade) {
        return isuserAdmin || contemPermissao(funcionalidade);
    }

    public Boolean contemPermissao(Funcionalidade funcionalidade) {
        if (Objects.isNull(funcionalidade)) {
            return Boolean.FALSE;
        }

        AuthIdentifier usuarioLogado = AuthIdentifierContext.getCurrentAuthIdentifier();
        return usuarioLogado.getFuncionalidadesPorModulo().entrySet().stream().anyMatch(entry ->
                entry.getKey() == Modulo.USUARIO && entry.getValue().contains(funcionalidade));
    }

    protected MenuAcao getAcaoNovo() {
        return MenuAcao
                .builder()
                .label("Novo")
                .funcao("novo")
                .exibirComoBotao(true)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CADASTRAR)).build();
    }

    protected MenuAcao getAcaoExibirFiltro() {
        return MenuAcao.builder()
                .icon("fa fa-search")
                .funcao("_exibirFiltros")
                .exibirComoBotao(true)
                .build();
    }

    protected MenuAcao getAcaoEditar() {
        return MenuAcao
                .builder()
                .label(LABEL_EDITAR)
                .funcao("habilitarEdicao")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoExcluir() {
        return MenuAcao
                .builder()
                .label(LABEL_EXCLUIR)
                .funcao("excluir")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EXCLUIR)).build();
    }

    protected MenuAcao getAcaoPreVisualizar() {
        return MenuAcao
                .builder()
                .label("Pré Visualizar")
                .funcao("preVisualizar")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CADASTRAR, Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoExportar() {
        return MenuAcao.builder()
                .label("Exportar")
                .funcao("exportar")
                .exibirComoBotao(true).build();
    }

    protected MenuAcao getAcaoVoltar() {
        return MenuAcao
                .builder()
                .label("Voltar")
                .funcao("voltar")
                .styleClassBotao("btn-kayros-bg-white-bd-green")
                .exibirComoBotao(true)
                .build();
    }

    protected MenuAcao getAcaoSalvar() {
        return MenuAcao
                .builder()
                .label("Salvar")
                .submit(true)
                .exibirComoBotao(true)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CADASTRAR, Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoExecutar() {
        return MenuAcao
                .builder()
                .label("Executar")
                .submit(true)
                .exibirComoBotao(true)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CADASTRAR, Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoCancelar() {
        return MenuAcao
                .builder()
                .label("Cancelar")
                .funcao("cancelar")
                .styleClassBotao("btn-kayros-bg-white-bd-green")
                .exibirComoBotao(true)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CANCELAR))
                .build();
    }


    protected MenuAcao getAcaoAtivar() {
        return MenuAcao
                .builder()
                .label(LABEL_ATIVAR)
                .funcao("ativar")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    protected MenuAcao getAcaoInativar() {
        return MenuAcao
                .builder()
                .label(LABEL_INATIVAR)
                .funcao("inativar")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    protected MenuAcao getAcaoConsultarTabelaListagem() {
        return MenuAcao
                .builder()
                .icon("fas fa-search")
                .label("Visualizar")
                .funcao("consultar")
                .funcionalidades(MenuAcao.toSet(Funcionalidade.CONSULTAR)).build();
    }

    protected MenuAcao getAcaoEditarTabelaListagem() {
        return MenuAcao
                .builder()
                .icon("fas fa-edit")
                .label(LABEL_EDITAR)
                .funcao(LABEL_EDITAR.toLowerCase())
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoEditarTabelaListagem(boolean desabilitado) {
        return MenuAcao
                .builder()
                .icon("fas fa-edit")
                .label(LABEL_EDITAR)
                .funcao(LABEL_EDITAR.toLowerCase())
                .desabilitado(desabilitado)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EDITAR)).build();
    }

    protected MenuAcao getAcaoExcluirTabelaListagem() {
        return MenuAcao
                .builder()
                .icon("fas fa-trash-alt")
                .label(LABEL_EXCLUIR)
                .funcao(LABEL_EXCLUIR.toLowerCase())
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EXCLUIR)).build();
    }

    protected MenuAcao getAcaoExcluirTabelaListagem(boolean desabilitado) {
        return MenuAcao
                .builder()
                .icon("fas fa-times")
                .label(LABEL_EXCLUIR)
                .funcao(LABEL_EXCLUIR.toLowerCase())
                .desabilitado(desabilitado)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.EXCLUIR)).build();
    }

    protected MenuAcao getAcaoAtivarTabelaListagem(boolean desabilitado) {
        return MenuAcao.builder()
                .icon("fas fa-thumbs-up")
                .label(LABEL_ATIVAR)
                .funcao(LABEL_ATIVAR.toLowerCase())
                .desabilitado(desabilitado)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    protected MenuAcao getAcaoAtivarTabelaListagem() {
        return MenuAcao.builder()
                .icon("fas fa-unlock")
                .label(LABEL_ATIVAR)
                .funcao(LABEL_ATIVAR.toLowerCase())
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    protected MenuAcao getAcaoInativarTabelaListagem() {
        return MenuAcao.builder()
                .icon("fas fa-lock")
                .label(LABEL_INATIVAR)
                .funcao(LABEL_INATIVAR.toLowerCase())
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    protected MenuAcao getAcaoInativarTabelaListagem(boolean desabilitado) {
        return MenuAcao.builder()
                .icon("fas fa-lock")
                .label(LABEL_INATIVAR)
                .funcao(LABEL_INATIVAR.toLowerCase())
                .desabilitado(desabilitado)
                .funcionalidades(MenuAcao.toSet(Funcionalidade.ATIVAR_INATIVAR)).build();
    }

    public abstract Modulo getModulo();
}
