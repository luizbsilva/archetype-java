package br.com.kayros.arquitetura.util;

import br.com.kayros.arquitetura.i18n.MensagemI18N;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class DateUtil {

    public static final Locale localeDefault = new Locale.Builder().setLanguage("pt").setRegion("BR").build();

    public static Date hoje() {
        return new Date();
    }

    public static String getDia(Date data) {

        String dia = "";

        if (!Objects.isNull(data)) {

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(data);

            dia = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        }

        return dia;
    }

    public static int getAno(Date data) {

        java.time.LocalDate localDate = data.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        return localDate.getYear();
    }

    public static Date addMinutos(Date date, int minutos) {

        Calendar c = Calendar.getInstance();

        c.setTime(date);

        c.add(Calendar.MINUTE, minutos);

        c.set(Calendar.SECOND, 0);

        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static int addAno(Date date, int qntd) {

        Calendar c = Calendar.getInstance();

        c.setTime(date);

        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + qntd);

        return c.get(Calendar.YEAR);
    }

    public static String formataDataHora(Date date) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, localeDefault);
        return dateFormat.format(date);
    }

    public static String formataData(Date date) {
        return formataData(date, DateFormat.MEDIUM);
    }

    public static String formataData(Date date, int style) {
        String valorFormatado = "";

        if (!Objects.isNull(date)) {
            DateFormat dateFormat = DateFormat.getDateInstance(style, localeDefault);
            valorFormatado = dateFormat.format(date);
        }

        return valorFormatado;
    }

    public static String getNomeMesDaData(Date date) {
        return getNomeMesDaData(date, false);
    }

    public static String getNomeMesDaData(Date date, boolean abreviado) {
        String mes = "";

        if (!Objects.isNull(date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            switch (calendar.get(Calendar.MONTH)) {
                case Calendar.JANUARY:
                    mes = MensagemI18N.getKey(abreviado ? "geral.jan" : "geral.janeiro");
                    break;
                case Calendar.FEBRUARY:
                    mes = MensagemI18N.getKey(abreviado ? "geral.fev" : "geral.fevereiro");
                    break;
                case Calendar.MARCH:
                    mes = MensagemI18N.getKey(abreviado ? "geral.mar" : "geral.marco");
                    break;
                case Calendar.APRIL:
                    mes = MensagemI18N.getKey(abreviado ? "geral.abr" : "geral.abril");
                    break;
                case Calendar.MAY:
                    mes = MensagemI18N.getKey(abreviado ? "geral.mai" : "geral.maio");
                    break;
                case Calendar.JUNE:
                    mes = MensagemI18N.getKey(abreviado ? "geral.jun" : "geral.junho");
                    break;
                case Calendar.JULY:
                    mes = MensagemI18N.getKey(abreviado ? "geral.jul" : "geral.julho");
                    break;
                case Calendar.AUGUST:
                    mes = MensagemI18N.getKey(abreviado ? "geral.ago" : "geral.agosto");
                    break;
                case Calendar.SEPTEMBER:
                    mes = MensagemI18N.getKey(abreviado ? "geral.set" : "geral.setembro");
                    break;
                case Calendar.OCTOBER:
                    mes = MensagemI18N.getKey(abreviado ? "geral.out" : "geral.outubro");
                    break;
                case Calendar.NOVEMBER:
                    mes = MensagemI18N.getKey(abreviado ? "geral.nov" : "geral.novembro");
                    break;
                case Calendar.DECEMBER:
                    mes = MensagemI18N.getKey(abreviado ? "geral.dez" : "geral.dezembro");
                    break;
            }

        }

        return mes;
    }

    public static String getNomeDiaSemana(Date data) {
        return getNomeDiaSemana(data, false);
    }

    public static String getNomeDiaSemana(Date data, Boolean abreviado) {
        String diaSemana = "";

        if (!Objects.isNull(data)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(data);

            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.SUNDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.dom" : "geral.domingo");
                    break;
                case Calendar.MONDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.seg" : "geral.segunda");
                    break;
                case Calendar.TUESDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.ter" : "geral.terca");
                    break;
                case Calendar.WEDNESDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.qua" : "geral.quarta");
                    break;
                case Calendar.THURSDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.qui" : "geral.quinta");
                    break;
                case Calendar.FRIDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.sex" : "geral.sexta");
                    break;
                case Calendar.SATURDAY:
                    diaSemana = MensagemI18N.getKey(abreviado ? "geral.sab" : "geral.sabado");
                    break;
            }
        }

        return diaSemana;
    }

    public static int compararDatas(Date data_1, Date data_2) {
        return compararDatas(data_1, data_2, false);
    }

    public static int compararDatas(Date data_1, Date data_2, boolean considerarHoras) {
        Calendar calendar_1 = Calendar.getInstance();
        calendar_1.setTime(data_1);

        Calendar calendar_2 = Calendar.getInstance();
        calendar_2.setTime(data_2);

        if (!considerarHoras) {
            zerarHora(calendar_1);
            zerarHora(calendar_2);
        }

        return calendar_1.compareTo(calendar_2);
    }

    public static Date ultimaHoraData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        ultimaHora(calendar);

        return calendar.getTime();
    }

    public static Date primeiroDiaData(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        definirDia(calendar, 1);

        return calendar.getTime();
    }

    public static Date ultimoDiaData(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        definirDia(calendar, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        return calendar.getTime();
    }

    private static void definirDia(Calendar calendar, int dia) {
        calendar.set(Calendar.DAY_OF_MONTH, dia);
    }

    public static Date zerarHoraData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        zerarHora(calendar);

        return calendar.getTime();
    }

    private static void zerarHora(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private static void ultimaHora(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public static ZonedDateTime getZoneDateTimFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return ZonedDateTime.of(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND),
                calendar.get(Calendar.MILLISECOND),
                ZoneId.of("America/Sao_Paulo"));
    }

    public static Integer getDiasEntreDatas(Date dataInicial, Date dataFinal) {
        int dias = 0;

        if (Objects.nonNull(dataInicial) && Objects.nonNull(dataFinal)) {
            Calendar calendarInicial = Calendar.getInstance();
            calendarInicial.setTime(dataInicial);

            Calendar calendarFinal = Calendar.getInstance();
            calendarFinal.setTime(dataFinal);

            dias = Days.daysBetween(LocalDate.fromCalendarFields(calendarInicial), LocalDate.fromCalendarFields(calendarFinal)).getDays();
        }

        return dias;
    }

    public static Date somarDiasAData(Date data, int dias) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.DATE, dias);
        return c.getTime();
    }

    public static int getDiferencaHorasEntreDuasDatas(Date dateStart, Date dateEnd) {
        Calendar calendarStart = Calendar.getInstance(), calendarEnd = Calendar.getInstance();
        calendarStart.setTime(dateStart);
        calendarEnd.setTime(dateEnd);
        return (calendarEnd.get(Calendar.HOUR_OF_DAY) - calendarStart.get(Calendar.HOUR_OF_DAY));
    }

    public static int getDiferencaMinutosEntreDuasDatas(Date dateStart, Date dateEnd) {
        Calendar calendarStart = Calendar.getInstance(), calendarEnd = Calendar.getInstance();
        calendarStart.setTime(dateStart);
        calendarEnd.setTime(dateEnd);
        return (calendarEnd.get(Calendar.MINUTE) - calendarStart.get(Calendar.MINUTE)) + (getDiferencaHorasEntreDuasDatas(dateStart, dateEnd) * 60);
    }

    public static Date somarHorasAData(Date data, int horas) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.HOUR, horas);
        return c.getTime();
    }

    public static Date somarMinutosAData(Date data, int minutos) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.MINUTE, minutos);
        return c.getTime();
    }

    public static Date converterDateStringParaDate(String data) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.parse(data);
    }

    public static String converterDataForStringFormatoMMDDyyyy_hhmmss(Date data) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
        return formatter.format(data);
    }
}
