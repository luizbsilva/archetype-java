package br.com.kayros.arquitetura.enums;

public enum ConfiguracaoEmailProtocolo {
    SSL, TLS
}
