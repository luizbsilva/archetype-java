package br.com.kayros.arquitetura.enums;

public enum Pagina {
    LISTAGEM,
    CADASTRO,
    CONSULTA
}
