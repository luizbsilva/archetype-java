package br.com.kayros.arquitetura.modelo;

public interface Arquivo extends Entidade {

    String getNome();

    void setNome(String nome);

    String getMimeType();

    void setMimeType(String mimeType);

    String getBase64();

    void setBase64(String base64);
}
