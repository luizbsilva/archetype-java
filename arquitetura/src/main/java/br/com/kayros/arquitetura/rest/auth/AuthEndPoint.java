package br.com.kayros.arquitetura.rest.auth;

import br.com.kayros.arquitetura.rest.BaseEndPoint;
import br.com.kayros.arquitetura.service.impl.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.file.AccessDeniedException;

public abstract class AuthEndPoint extends BaseEndPoint {

    private static final String REQUEST_PARAMETER_USERNAME = "username";
    private static final String REQUEST_PARAMETER_SENHA = "password";

    @Context
    protected HttpServletRequest request;

    @Autowired
    private AuthService authService;

    @POST
    @PermitAll
    public Response auth() {
        final String usuario = this.request.getParameter(AuthEndPoint.REQUEST_PARAMETER_USERNAME);
        final String senha = this.request.getParameter(AuthEndPoint.REQUEST_PARAMETER_SENHA);

        try {
            AuthIdentifier usuarioLogado = processarLogin(usuario, senha);
            usuarioLogado.setToken(authService.generateToken(usuarioLogado));

            return this.createResponseAuth(usuarioLogado);

        } catch (AccessDeniedException e) {
            return Response.status(Response.Status.UNAUTHORIZED.getStatusCode(), e.getMessage()).build();

        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), e.getMessage()).build();

        }
    }

    private Response createResponseAuth(AuthIdentifier authIdentifier) {
        return Response.ok().entity(authIdentifier).type(MediaType.APPLICATION_JSON).build();
    }

    protected abstract AuthIdentifier processarLogin(String username, String password) throws AccessDeniedException;
}
