package br.com.kayros.arquitetura.rest;

import br.com.kayros.arquitetura.dto.MenuAcao;
import br.com.kayros.arquitetura.dto.PaginacaoResultado;
import br.com.kayros.arquitetura.enums.Pagina;
import br.com.kayros.arquitetura.i18n.MensagemI18N;
import br.com.kayros.arquitetura.modelo.ObjectID;
import br.com.kayros.arquitetura.paginacao.Paginacao;
import br.com.kayros.arquitetura.service.ServiceConsultar;
import br.com.kayros.arquitetura.util.RegraNegocioException;
import br.com.kayros.arquitetura.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;
import java.util.Map;


public abstract class ConsultarEndPoint<E extends ObjectID, ID extends Serializable> extends BaseEndPoint {

    @POST
    @Path("/listar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PaginacaoResultado listar(Paginacao paginacao) {
        return getServico().dtoListar(paginacao);
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<E> listar() {
        return this.getServico().listar();
    }

    @SuppressWarnings("RestParamTypeInspection")
    @GET
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response dtoGet(@PathParam("id") Long id) {
        E entidade = getServico().get(id);

        return gerarResponseDeConsultarEntidade(entidade);
    }

    @POST
    @Path("/acoes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<MenuAcao> acoes(Map<String, Object> parametros) {
        ObjectMapper mapper = new ObjectMapper();
        E entidade = mapper.convertValue(parametros.get("entidade"), (Class<E>) Util.getTypes(this)[0]);
        Pagina pagina = mapper.convertValue(parametros.get("pagina"), Pagina.class);

        return getServico().getAcoes(entidade, pagina);
    }

    @POST
    @Path("/acoes/tabelaListagem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<MenuAcao> acoesTabelaListagem(E entidade) {
        return getServico().getAcoesTabelaListagem(entidade);
    }

    protected Response gerarResponseDeConsultarEntidade(E entidade) {
        return gerarResponseDeConsultarEntidade(entidade, Response.Status.NOT_FOUND);
    }

    protected Response gerarResponseDeConsultarEntidade(E entidade, Response.Status statusOnErro) {
        Response response;

        if (entidade != null) {
            executarAposConsultar(entidade);

            response = Response.ok(entidade).build();

        } else {
            RegraNegocioException regraNegocioException = new RegraNegocioException();
            regraNegocioException.addMensagem(MensagemI18N.getKey("operacao.consultar.falha"));

            response = gerarResponseDeRegraNegocioException(statusOnErro, regraNegocioException);
        }

        return response;
    }

    public void executarAposConsultar(E entidade) {
    }

    protected abstract ServiceConsultar<E> getServico();

}
