package br.com.kayros.arquitetura.modelo.impl;

import br.com.kayros.arquitetura.enums.Status;
import br.com.kayros.arquitetura.modelo.Entidade;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@Getter
@Setter
@MappedSuperclass
public class EntidadeImpl implements Entidade {

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated
    private Status status = Status.ATIVO;

    public Status getStatus() {
        if (Objects.isNull(status)) {
            ativar();
        }

        return status;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;

        try {
            equals = ((EntidadeImpl) obj).getId().equals(getId());

        } catch (Exception e) {
            equals = super.equals(obj);

        }

        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode;

        try {
            hashCode = id.hashCode();

        } catch (Exception e) {
            hashCode = super.hashCode();

        }

        return hashCode;
    }
}
