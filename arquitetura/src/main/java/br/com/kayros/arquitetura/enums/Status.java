package br.com.kayros.arquitetura.enums;

public enum Status {
    INATIVO,
    ATIVO;

    public Status ifAtivo(Runnable runnable) {
        return check(ATIVO, runnable);
    }

    public Status ifInativo(Runnable runnable) {
        return check(INATIVO, runnable);
    }

    private Status check(Status e, Runnable runnable) {
        if (this.equals(e)) {
            runnable.run();
        }
        return this;
    }
}
