package br.com.kayros.arquitetura.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Classe responsável por fazer o controle de execuções paralelas:<br/>
 * <br/>
 * <b>Métodos disponíveis:</b><br/>
 * {@link ThreadPool#enfileirarTarefa}
 * <br/>
 * {@link ThreadPool#executarTarefa}
 * <br/>
 * {@link ThreadPool#enfileirarTarefaFixed}
 */
public class ThreadPool {

    private static ThreadPool instance;

    private static ThreadPool getInstance() {
        if (instance == null) {
            instance = new ThreadPool();
        }
        return instance;
    }

    private ExecutorService executorServiceSingle;
    private ExecutorService executorServiceCached;
    private ExecutorService executorServiceFixed;

    private ThreadPool() {
        executorServiceSingle = Executors.newSingleThreadExecutor();
        executorServiceCached = Executors.newCachedThreadPool();
        executorServiceFixed = Executors.newFixedThreadPool(10);
    }

    /**
     * Adiciona uma tarefa na fila de Execução. Nesse caso o sistema vai executar uma a uma por ordem de chegada em uma thread separada.
     *
     * @param tarefa um Runnable
     * @see ThreadPool
     */
    public static void enfileirarTarefa(Runnable tarefa) {
        getInstance().executorServiceSingle.execute(tarefa);
    }

    /**
     * Executa uma tarefa imediatamente em uma thread separada.
     *
     * @param tarefa um Runnable
     * @see ThreadPool
     */
    public static void executarTarefa(Runnable tarefa) {
        getInstance().executorServiceCached.execute(tarefa);
    }

    /**
     * Adiciona uma tarefa na fila de Execução. No entando, nesse caso o sistema vai permitir a execução de até 10 thread simultâneas, somente ao exceder as 10
     * que o sistema faz a gestão em fila.
     *
     * @param tarefa um Runnable
     * @see ThreadPool
     */
    public static void enfileirarTarefaFixed(Runnable tarefa) {
        getInstance().executorServiceFixed.execute(tarefa);
    }
}
