package br.com.kayros.arquitetura.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class PaginacaoResultado<T> {

    private List<T> entidades;
    private long quantidade;

    private Map<String, List<MenuAcao>> acoesTabelaListagem = new HashMap<>();

    public PaginacaoResultado(List<T> entidades, long quantidade) {
        this.entidades = entidades;
        this.quantidade = quantidade;
    }

    public static <T> PaginacaoResultado<T> paginacaoResultadoEmpty() {
        return new PaginacaoResultado<>(new ArrayList<>(), 0);
    }

    public void addAcoesTabelaListagem(String identificador, List<MenuAcao> acoes) {
        acoesTabelaListagem.put(identificador, acoes);
    }
}
